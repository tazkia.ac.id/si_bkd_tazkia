package id.tazkia.sibkd.services;

import id.tazkia.sibkd.dto.RangkumanBkdDto;

import java.util.List;

public interface RangkumanBkdService {

    void addRangkumanBkd(RangkumanBkdDto rangkumanBkdDto);
    List<RangkumanBkdDto> getRangkumanBkd();

}

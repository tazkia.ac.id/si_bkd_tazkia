package id.tazkia.sibkd.services;

import id.tazkia.sibkd.dao.config.RoleDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.config.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUserService.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    public User currentUser(Authentication currentUser){

        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) currentUser;

        String username = (String) token.getPrincipal().getAttributes().get("email");

        User u = userDao.findByUsername(username);

        return u;

    }

    public User createUser(String nama, String email, String role){
        User user = new User();
        user.setUsername(email);
        user.setNamaLengkap(nama);
        user.setRole(roleDao.findById(role).get());
        user.setActive(Boolean.TRUE);
        userDao.save(user);

        return user;
    }

    public User updateUser(User user, String email, String nama){
        user.setUsername(email);
        userDao.save(user);

        return user;
    }

}

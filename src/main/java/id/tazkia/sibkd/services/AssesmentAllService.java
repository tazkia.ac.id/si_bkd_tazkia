package id.tazkia.sibkd.services;

import id.tazkia.sibkd.dao.AssesmentAllDao;
import id.tazkia.sibkd.dao.AssesmentAllDetailDao;
import id.tazkia.sibkd.dao.LaporanBkdDao;
import id.tazkia.sibkd.entity.AssesmentAll;
import id.tazkia.sibkd.entity.AssesmentAllDetail;
import id.tazkia.sibkd.entity.LaporanBkd;
import id.tazkia.sibkd.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


@Service
@EnableScheduling
public class AssesmentAllService {


    @Autowired
    private AssesmentAllDao assesmentAllDao;

    @Autowired
    private LaporanBkdDao laporanBkdDao;

    @Autowired
    private AssesmentAllDetailDao assesmentAllDetailDao;

    @Scheduled(fixedDelay = 1000)
    public void ambilDataAssesmentAll(){

        AssesmentAll assesmentAll = assesmentAllDao.findFirstByStatusAndStatusAssesmentOrderByTanggal(StatusRecord.AKTIF, StatusRecord.WAITING);
        if(assesmentAll != null){
            System.out.println("Assesment All Jalan");
            assesmentAll.setMulai(LocalDateTime.now());
            assesmentAll.setStatusAssesment(StatusRecord.ON_PROCESS);
            assesmentAllDao.save(assesmentAll);

            //Looping laporan bkd yang belum dinilai oleh asesor 1
            List<LaporanBkd> laporanBkdList = laporanBkdDao.laporanBkdBelumAsesorSatu(StatusRecord.AKTIF, assesmentAll.getSemester());
            if(laporanBkdList != null){
                System.out.println("Penilaian asesor satu");
                for (LaporanBkd laporanBkd : laporanBkdList){
                    System.out.println("Dosen : "+ laporanBkd.getDosen().getNama());
                    laporanBkd.setSksTerpenuhiAsesor1(laporanBkd.getSksTerpenuhi());
                    laporanBkd.setCatatanAsesor1(assesmentAll.getCatatan());
                    laporanBkd.setRekomendasiAsesor1(assesmentAll.getRekomendasi());
                    laporanBkd.setTanggalAsesor1(LocalDateTime.now());
                    laporanBkdDao.save(laporanBkd);
                    AssesmentAllDetail assesmentAllDetail = new AssesmentAllDetail();
                    assesmentAllDetail.setAssesmentAll(assesmentAll);
                    assesmentAllDetail.setNamaDosen(laporanBkd.getDosen().getNama());
                    assesmentAllDetail.setKeterangan("Assesor 1");
                    assesmentAllDetail.setStatus(StatusRecord.AKTIF);
                    assesmentAllDetail.setStatusDetail(StatusRecord.BERHASIL);
                    assesmentAllDetailDao.save(assesmentAllDetail);
                }
            }

            List<LaporanBkd> laporanBkdList2 = laporanBkdDao.laporanBkdBelumAsesorDua(StatusRecord.AKTIF, assesmentAll.getSemester());
            if(laporanBkdList2 != null){
                System.out.println("Penilaian asesor dua");
                for (LaporanBkd laporanBkd2 : laporanBkdList2){
                    System.out.println("Dosen : "+ laporanBkd2.getDosen().getNama());
                    laporanBkd2.setSksTerpenuhiAsesor2(laporanBkd2.getSksTerpenuhi());
                    laporanBkd2.setCatatanAsesor2(assesmentAll.getCatatan());
                    laporanBkd2.setRekomendasiAsesor2(assesmentAll.getRekomendasi());
                    laporanBkd2.setTanggalAsesor2(LocalDateTime.now());
                    laporanBkdDao.save(laporanBkd2);
                    AssesmentAllDetail assesmentAllDetail = new AssesmentAllDetail();
                    assesmentAllDetail.setAssesmentAll(assesmentAll);
                    assesmentAllDetail.setNamaDosen(laporanBkd2.getDosen().getNama());
                    assesmentAllDetail.setKeterangan("Assesor 2");
                    assesmentAllDetail.setStatus(StatusRecord.AKTIF);
                    assesmentAllDetail.setStatusDetail(StatusRecord.BERHASIL);
                    assesmentAllDetailDao.save(assesmentAllDetail);
                }
            }

            assesmentAll.setStatusAssesment(StatusRecord.DONE);
            assesmentAll.setSelesai(LocalDateTime.now());
            assesmentAllDao.save(assesmentAll);
        }
    }

}

package id.tazkia.sibkd.entity.config;


import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "s_user")
//@Data
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String username;
    private Boolean active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

    private String namaLengkap;

    private String userUpdate;

    private LocalDateTime dateUpdate;

//    @ManyToOne
//    @JoinColumn(name = "id_perguruan_tinggi")
//    private PerguruanTinggi perguruanTinggi;

}

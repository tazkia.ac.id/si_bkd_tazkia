package id.tazkia.sibkd.entity.config;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "s_user_password")
@Data
public class UserPassword {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @NotEmpty
    private String password;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

}

package id.tazkia.sibkd.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@EqualsAndHashCode(of = {"id", "laporanBkd"})
@Data
public class LaporanBkdFile {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaFile;

    private String extensi;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    @ManyToOne
    @JoinColumn(name = "id_laporan_bkd")
    private LaporanBkd laporanBkd;

    @Enumerated(EnumType.STRING)
    private JenisFileLaporan jenisFileLaporan;



}

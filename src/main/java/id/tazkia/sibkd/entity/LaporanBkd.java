package id.tazkia.sibkd.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.GenericGenerator;

import id.tazkia.sibkd.entity.config.User;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class LaporanBkd {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Dosen dosen;

    @Enumerated(EnumType.STRING)
    private Bidang bidang;

    @ManyToOne
    @JoinColumn(name = "id_kegiatan")
    private Kegiatan kegiatan;

    @ManyToOne
    @JoinColumn(name = "id_semester")
    private Semester semester;

    private String jenisKegiatan;

    private String namaKegiatan;

    private String buktiPenugasan;
    private BigDecimal jumlahSks;
    private BigDecimal masaPenugasan;
    private String buktiKinerja;
    private BigDecimal sksTerpenuhi;
    @Enumerated(EnumType.STRING)
    private StatusRecord laporan;
    @Enumerated(EnumType.STRING)
    private StatusRecord perbaikan;
    private BigDecimal sksTerpenuhiAsesor1;
    private BigDecimal sksTerpenuhiAsesor2;
    private String catatanAsesor1;
    private String catatanAsesor2;
    @Enumerated(EnumType.STRING)
    private Rekomendasi rekomendasiAsesor1;
    @Enumerated(EnumType.STRING)
    private Rekomendasi rekomendasiAsesor2;
    private LocalDateTime tanggalAsesor1;
    private LocalDateTime tanggalAsesor2;
    @Enumerated(EnumType.STRING)
    private StatusRecord status;
    private LocalDateTime tanggalInsert;

    private LocalDateTime tanggalUpdate;

    private String url;

    @ManyToOne
    @JoinColumn(name = "asesor1")
    private User asesor1;

    @ManyToOne
    @JoinColumn(name = "asesor2")
    private User asesor2;

    @OneToMany(mappedBy = "laporanBkd", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LaporanBkdFile> laporanBkdFiles = new HashSet<>();

}

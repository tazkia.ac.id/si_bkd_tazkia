package id.tazkia.sibkd.entity;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@Entity
@Data
public class JabatanStruktural {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String namaJabatanStruktural;

    @NotNull
    private Integer batasBawahPendidikan;

    @NotNull
    private Integer batasAtasPendidikan;

    @NotNull
    private Integer batasBawahPenelitian;

    @NotNull
    private Integer batasAtasPenelitian;

    @NotNull
    private Integer batasBawahPendidikanPenelitian;

    @NotNull
    private Integer batasBawahPengabdian;

    @NotNull
    private Integer batasAtasPengabdian;

    @NotNull
    private Integer batasBawahPenunjang;

    @NotNull
    private Integer batasBawahPengabdianPenunjang;

    @NotNull
    private Integer batasAtasPenunjang;

    @NotNull
    private Integer batasBawahTotal;

    @NotNull
    private Integer batasAtasTotal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}

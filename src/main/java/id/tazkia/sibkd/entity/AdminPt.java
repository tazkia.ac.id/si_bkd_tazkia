package id.tazkia.sibkd.entity;

import id.tazkia.sibkd.entity.config.User;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "admin_perguruan_tinggi")
public class AdminPt {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_perguruan_tinggi")
    private PerguruanTinggi perguruanTinggi;

    @NotNull
    @Column(name = "nama_lengkap")
    private String nama;

    @NotNull
    @Email
    private String email;

    private String telepon;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}

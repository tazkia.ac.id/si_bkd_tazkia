package id.tazkia.sibkd.entity;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class AssesmentAll {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private LocalDateTime tanggal;

    private String user;

    private String catatan;

    @Enumerated(EnumType.STRING)
    private Rekomendasi rekomendasi;

    @ManyToOne
    @JoinColumn(name = "id_semester")
    private Semester semester;

    private LocalDateTime mulai;

    private LocalDateTime selesai;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAssesment;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

}

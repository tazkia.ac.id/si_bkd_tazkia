package id.tazkia.sibkd.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class DosenDetail extends Auditable{
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Dosen dosen;

    private String nidn;
    private String foto;

    private String kartuNidn;

    private String nomorSertifikat;

    private String sertifikatDosen;

    private String fakultas;

    @ManyToOne
    @JoinColumn(name = "prodi")
    private ProgramStudi prodi;

    private String skDt;

    private String jabatanFungsional;

    private String skJafung;

    private String ijazahs1;

    private String ijazahs2;

    private String ijazahs3;
    private String perguruans1;
    private String perguruans2;
    private String perguruans3;

    private String namaSertifikatDosen;
    private String namaKartuNidn;
    private String namaFoto;
    private String namaJafung;
    private String namaSkdt;
    private String namaIjazahs1;
    private String namaIjazahs2;
    private String namaIjazahs3;

    @Lob
    private String alamat;

    @Lob
    private String judulSkripsi;

    @Lob
    private String judulTesis;

    @Lob
    private String judulDisertasi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}

package id.tazkia.sibkd.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class AssesmentAllDetail {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_assesment_all")
    private AssesmentAll assesmentAll;

    private String namaDosen;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusDetail;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    private String keterangan;

}

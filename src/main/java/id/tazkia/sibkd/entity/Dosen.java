package id.tazkia.sibkd.entity;

import org.hibernate.annotations.GenericGenerator;

import id.tazkia.sibkd.entity.config.User;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

//
@Data
@Entity
public class Dosen {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_perguruan_tinggi")
    private PerguruanTinggi perguruanTinggi;

    @ManyToOne
    @JoinColumn(name = "status_jabatan")
    private JabatanStruktural jabatanStruktural;

    @NotNull
    @Column(name = "nama_lengkap")
    private String nama;

    @NotNull
    @Email
    private String email;

    private String telepon;

    @ManyToOne
    @JoinColumn(name = "id_asesor_satu")
    private User asesorSatu;

    @ManyToOne
    @JoinColumn(name = "id_asesor_dua")
    private User asesorDua;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}

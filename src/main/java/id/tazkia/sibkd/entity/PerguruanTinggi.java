package id.tazkia.sibkd.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class PerguruanTinggi {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaPerguruanTinggi;

    private String alamat;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDateTime tanggalInsert;
    private LocalDateTime tanggalUpdate;
    private LocalDateTime tanggalDelete;

    private String userInsert;
    private String userUpdate;
    private String userDelete;

}

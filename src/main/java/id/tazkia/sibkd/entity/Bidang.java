package id.tazkia.sibkd.entity;

public enum Bidang {

    PENDIDIKAN, PENELITIAN, PENGABDIAN, PENUNJANG

}

package id.tazkia.sibkd.dto;

public interface ListAssesmentDto {

    String getIdDosen();
    String getNamaDosen();
    String getStatus();

}

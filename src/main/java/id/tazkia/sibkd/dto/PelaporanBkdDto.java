package id.tazkia.sibkd.dto;

public interface PelaporanBkdDto {

    String getId();
    String getNama();
    String getPerguruanTinggi();
    String getAsesorSatu();
    String getAsesorDua();
    String getStatus();

}

package id.tazkia.sibkd.dto;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;

public interface RangkumanBkdDto {

    String getNomor();
    String getKegiatan();
    String getKetentuan();

    BigDecimal getKinerja();

    String getDosenTercapai();

    BigDecimal getSksTerpenuhiAsesor1();

    BigDecimal getSksTerpenuhiAsesor2();

    String getAsesorTercapai1();

    String getAsesorTercapai2();

    String getStatusAkhir();


}

package id.tazkia.sibkd.dto;

import id.tazkia.sibkd.entity.JabatanStruktural;
import id.tazkia.sibkd.entity.PerguruanTinggi;
import id.tazkia.sibkd.entity.ProgramStudi;
import jakarta.persistence.Lob;
import lombok.Data;

@Data
public class BiodataDto {
    private String id;
    private String idDosen;
    private String nama;
    private String email;
    private String nidn;
    private String profil;
    private String namaSertifikatDosen;
    private String namaKartuNidn;
    private String namaFoto;
    private String namaJafung;
    private String namaSkdt;
    private String namaIjazahs1;
    private String namaIjazahs2;
    private String namaIjazahs3;

//    private String kartuNidn;

    private String nomorSertifikat;

//    private String sertifikatDosen;

    private PerguruanTinggi perguruanTinggi;

    private String fakultas;

    private ProgramStudi prodi;
    private String namaProdi;

    private String perguruans1;
    private String perguruans2;
    private String perguruans3;

    private String telepon;

    private JabatanStruktural jabatanStruktural;

//    private String skDt;

    private String jabatanFungsional;

//    private String skJafung;

//    private String ijazahs1;

//    private String ijazahs2;

//    private String ijazahs3;

    @Lob
    private String alamat;

    @Lob
    private String judulSkripsi;

    @Lob
    private String judulTesis;

    @Lob
    private String judulDisertasi;
}

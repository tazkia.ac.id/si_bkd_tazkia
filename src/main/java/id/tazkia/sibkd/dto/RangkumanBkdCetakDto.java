package id.tazkia.sibkd.dto;

import lombok.Data;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@Data
public class RangkumanBkdCetakDto {

    private List<RangkumanBkdDto> rangkumanBkdDtos = new ArrayList<>();

}

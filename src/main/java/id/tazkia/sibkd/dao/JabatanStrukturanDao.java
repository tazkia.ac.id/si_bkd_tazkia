package id.tazkia.sibkd.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.JabatanStruktural;
import id.tazkia.sibkd.entity.StatusRecord;

public interface JabatanStrukturanDao extends PagingAndSortingRepository<JabatanStruktural, String>, CrudRepository<JabatanStruktural, String> {

    Page<JabatanStruktural> findByStatus(StatusRecord statusRecord, Pageable pageable);
}

package id.tazkia.sibkd.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.Semester;
import id.tazkia.sibkd.entity.StatusRecord;

public interface SemesterDao extends PagingAndSortingRepository<Semester, String>, CrudRepository<Semester, String> {
    List<Semester> findByStatusOrderByTanggalInsertDesc(StatusRecord statusRecord);
    List<Semester> findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord statusRecord);

    Semester findFirstByStatusOrderByTanggalBukaSemesterDesc(StatusRecord statusRecord);
    Semester findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord statusRecord, LocalDateTime tanggalBuka, LocalDateTime tanggalTutup);

    Semester findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord statusRecord,LocalDateTime tanggalTutup);

    Semester findTopByStatusAndTanggalTutupLaporanIsAfter(StatusRecord statusRecord, LocalDateTime tanggalTutupLaproan);

    @Query(value = "select count(id) as ada from semester where id = ?1 and (tanggal_buka_laporan <= now() and tanggal_tutup_laporan >= now()) or (tanggal_buka_perbaikan <= now() and tanggal_tutup_perbaikan >= now()) and status = 'AKTIF'", nativeQuery = true)
    Integer cariJadwalPelaporanAktif(String idSemester);

    @Query(value = "select count(id) as ada from semester where id = ?1 and (tanggal_buka_penilaian <= now() and tanggal_tutup_penilaian >= now()) or (tanggal_buka_penilaian_ulang <= now() and tanggal_tutup_penilaian_ulang >= now()) and status = 'AKTIF'", nativeQuery = true)
    Integer cariJadwalAssesmentAktif(String idSemester);

}

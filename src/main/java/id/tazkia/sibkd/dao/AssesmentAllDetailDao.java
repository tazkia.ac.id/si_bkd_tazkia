package id.tazkia.sibkd.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.AssesmentAll;
import id.tazkia.sibkd.entity.AssesmentAllDetail;
import id.tazkia.sibkd.entity.StatusRecord;

public interface AssesmentAllDetailDao extends PagingAndSortingRepository<AssesmentAllDetail, String>, CrudRepository<AssesmentAllDetail, String> {

    Page<AssesmentAllDetail> findByStatusAndAssesmentAllOrderByNamaDosen(StatusRecord statusRecord, AssesmentAll assesmentAll, Pageable pageable);

}

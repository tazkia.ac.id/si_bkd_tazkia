package id.tazkia.sibkd.dao;

import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Kegiatan;
import id.tazkia.sibkd.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KegiatanDao extends PagingAndSortingRepository<Kegiatan, String>, CrudRepository<Kegiatan, String> {

    Page<Kegiatan> findByStatusAndBidang(StatusRecord statusRecord, Bidang bidang, Pageable pageable);

    List<Kegiatan> findByStatusAndBidangOrderByNamaKegiatan(StatusRecord statusRecord, Bidang bidang);


}

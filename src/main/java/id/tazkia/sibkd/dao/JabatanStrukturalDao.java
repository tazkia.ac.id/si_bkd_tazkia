package id.tazkia.sibkd.dao;

import id.tazkia.sibkd.entity.JabatanStruktural;
import id.tazkia.sibkd.entity.StatusRecord;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JabatanStrukturalDao extends PagingAndSortingRepository<JabatanStruktural, String>, CrudRepository<JabatanStruktural, String> {
    List<JabatanStruktural> findByStatus(StatusRecord statusRecord);
}

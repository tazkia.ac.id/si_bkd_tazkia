package id.tazkia.sibkd.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.dto.ListAssesmentDto;
import id.tazkia.sibkd.dto.PelaporanBkdDto;
import id.tazkia.sibkd.dto.RangkumanBkdDto;
import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.LaporanBkd;
import id.tazkia.sibkd.entity.Semester;
import id.tazkia.sibkd.entity.StatusRecord;

public interface LaporanBkdDao extends PagingAndSortingRepository<LaporanBkd, String>, CrudRepository<LaporanBkd, String> {

    LaporanBkd findByStatusAndId(StatusRecord statusRecord, String id);

    List<LaporanBkd> findByStatusAndDosenAndBidangAndSemesterOrderByTanggalInsert(StatusRecord statusRecord, Dosen dosen, Bidang bidang, Semester semester);

    List<LaporanBkd> findByStatusAndDosenAndSemesterOrderByTanggalInsert(StatusRecord statusRecord, Dosen dosen, Semester semester);

    List<LaporanBkd> findByStatusAndDosenAndSemesterAndBidangOrderByTanggalInsert(StatusRecord statusRecord, Dosen dosen, Semester semester, Bidang bidang);

    @Query(value = "select a.*,if(asesorTercapai1 = 'M' or asesorTercapai2 = 'M','M','T') as statusAkhir from\n" +
            "(select 1 as nomor,'Pendidikan' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_pendidikan and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_pendidikan,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_pendidikan and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_pendidikan,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_pendidikan and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_atas_pendidikan,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,'PENDIDIKAN' as a,batas_bawah_pendidikan,batas_atas_pendidikan, if(batas_bawah_pendidikan > 0,'Tidak boleh kosong','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen and a.a = b.bidang\n" +
            "union\n" +
            "select 2 as nomor,'Penelitian' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_penelitian and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_penelitian,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_penelitian and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_penelitian,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_penelitian and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_atas_penelitian,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,'PENELITIAN' as a,batas_bawah_penelitian,batas_atas_penelitian, if(batas_bawah_penelitian > 0,'Tidak boleh kosong','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen and a.a = b.bidang\n" +
            "union\n" +
            "select 3 as nomor,'Pengabdian' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_pengabdian and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_pengabdian,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_pengabdian and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_pengabdian,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_pengabdian and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_bawah_pengabdian,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,'PENGABDIAN' as a,batas_bawah_pengabdian,batas_atas_pengabdian, if(batas_bawah_pengabdian > 0,'Tidak boleh kosong','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen and a.a = b.bidang\n" +
            "union\n" +
            "select 4 as nomor,'Penunjang' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_penunjang and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_penunjang,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_penunjang and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_penunjang,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_penunjang and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_atas_penunjang,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,'PENUNJANG' as a,batas_bawah_penunjang,batas_atas_penunjang, if(batas_bawah_penunjang > 0,'Tidak boleh kosong','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen and a.a = b.bidang\n" +
            "union\n" +
            "select 5 as nomor,'Pendidikan & Penelitian' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_pendidikan_penelitian and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_total,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_pendidikan_penelitian and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_total,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_pendidikan_penelitian and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_atas_total,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,'PENDIDIKAN' as a, 'PENELITIAN' as b,batas_bawah_pendidikan_penelitian,batas_atas_total, if(batas_bawah_pendidikan_penelitian > 0,'Sesuai Pedoman','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen and (a.a = b.bidang or a.b = b.bidang)\n" +
            "union\n" +
            "select 6 as nomor,'Pengabdian & Penunjang' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_pengabdian_penunjang and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_total,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_pengabdian_penunjang and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_total,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_pengabdian_penunjang and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_atas_total,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,'PENGABDIAN' as a, 'PENUNJANG' as b,batas_bawah_pengabdian_penunjang,batas_atas_total, if(batas_bawah_pengabdian_penunjang > 0,'Sesuai Pedoman','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen and (a.a = b.bidang or a.b = b.bidang)\n" +
            "union\n" +
            "select 7 as nomor,'Total Kinerja' as kegiatan, ketentuan, coalesce(sum(sks_terpenuhi),0) as kinerja, if(sum(coalesce(sks_terpenuhi,0)) >= batas_bawah_total and sum(coalesce(sks_terpenuhi,0)) <= batas_atas_total,'M','T') as dosenTercapai,\n" +
            "sum(coalesce(sks_terpenuhi_asesor1,0)) as sksTerpenuhiAsesor1, sum(coalesce(sks_terpenuhi_asesor2,0)) as sksTerpenuhiAsesor2,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor1,0)) >= batas_bawah_total and sum(coalesce(sks_terpenuhi_asesor1,0)) <= batas_atas_total,'M','T') as asesorTercapai1,\n" +
            "if(sum(coalesce(sks_terpenuhi_asesor2,0)) >= batas_bawah_total and sum(coalesce(sks_terpenuhi_asesor2,0)) <= batas_atas_total,'M','T') as asesorTercapai2 from\n" +
            "(select 1 as nomor,a.id as id_dosen,batas_bawah_total,batas_atas_total, if(batas_bawah_total > 0,'Sesuai Pedoman','Boleh kosong') as ketentuan from dosen as a\n" +
            "inner join jabatan_struktural as b on a.status_jabatan = b.id \n" +
            "where a.id = ?1) as a\n" +
            "left join (select * from laporan_bkd where id_dosen = ?1 and id_semester = ?2 and status = 'AKTIF') as b on a.id_dosen = b.id_dosen) as a",nativeQuery = true)
    List<RangkumanBkdDto> rangkumanBkd(String idDosen, String idSemester);



    @Query(value = "select id,nama,perguruanTinggi,asesorSatu,asesorDua,\n" +
            "if(asesorSatu = '-' and asesordua = '-', 'BA',if(laporan = 0, 'BI', if(asesorSatu <> '-' and asesorDua <> '-',if(laporanAsesor1 >= laporan and laporanAsesor2 >= laporan,'SE','BS'),\n" +
            "if(asesorSatu = '-', if(laporanAsesor2 >= laporan,'SE','BS'),if(laporanAsesor1 >= laporan,'SE','BS'))))) as status from\n" +
            "(select a.id,nama,perguruanTinggi,asesorSatu,asesorDua,coalesce(laporan,0) as laporan,coalesce(status_asesor_1,0) laporanAsesor1,coalesce(status_asesor_2,0) laporanAsesor2 from\n" +
            "(select a.id,a.nama_lengkap as nama,d.nama_perguruan_tinggi as perguruanTinggi,coalesce(b.nama_lengkap,'-') as asesorSatu,coalesce(c.nama_lengkap,'-') as asesorDua from dosen as a\n" +
            "inner join perguruan_tinggi as d on a.id_perguruan_tinggi = d.id\n" +
            "left join s_user as b on a.id_asesor_satu = b.id \n" +
            "left join s_user as c on a.id_asesor_dua = c.id where a.status = 'AKTIF' order by perguruanTinggi, nama) as a\n" +
            "left join \n" +
            "(select id_dosen,count(id)as laporan from laporan_bkd where status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as b on a.id = b.id_dosen \n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_1 from laporan_bkd where sks_terpenuhi_asesor1 is not null and status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as c on a.id = c.id_dosen \n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_2 from laporan_bkd where sks_terpenuhi_asesor2 is not null and status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as d on a.id = d.id_dosen)as a", nativeQuery = true)
    List<PelaporanBkdDto> pelaporanBkd(String Semester);

    @Query(value = "select * from (select id,nama,perguruanTinggi,asesorSatu,asesorDua,\n" +
            "if(asesorSatu = '-' and asesordua = '-', 'BA',if(laporan = 0, 'BI', if(asesorSatu <> '-' and asesorDua <> '-',if(laporanAsesor1 >= laporan and laporanAsesor2 >= laporan,'SE','BS'),\n" +
            "if(asesorSatu = '-', if(laporanAsesor2 >= laporan,'SE','BS'),if(laporanAsesor1 >= laporan,'SE','BS'))))) as status from\n" +
            "(select a.id,nama,perguruanTinggi,asesorSatu,asesorDua,coalesce(laporan,0) as laporan,coalesce(status_asesor_1,0) laporanAsesor1,coalesce(status_asesor_2,0) laporanAsesor2 from\n" +
            "(select a.id,a.nama_lengkap as nama,d.nama_perguruan_tinggi as perguruanTinggi,coalesce(b.nama_lengkap,'-') as asesorSatu,coalesce(c.nama_lengkap,'-') as asesorDua from dosen as a\n" +
            "inner join perguruan_tinggi as d on a.id_perguruan_tinggi = d.id\n" +
            "left join s_user as b on a.id_asesor_satu = b.id \n" +
            "left join s_user as c on a.id_asesor_dua = c.id where a.status = 'AKTIF' order by perguruanTinggi, nama) as a\n" +
            "left join \n" +
            "(select id_dosen,count(id)as laporan from laporan_bkd where status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as b on a.id = b.id_dosen \n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_1 from laporan_bkd where sks_terpenuhi_asesor1 is not null and status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as c on a.id = c.id_dosen \n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_2 from laporan_bkd where sks_terpenuhi_asesor2 is not null and status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as d on a.id = d.id_dosen)as a) b where status = ?2", nativeQuery = true)
    List<PelaporanBkdDto> pelaporanBkdDenganStatus(String Semester, String status);


    @Query(value = "select id,nama,perguruanTinggi,asesorSatu,asesorDua,\n" +
            "if(asesorSatu = '-' and asesordua = '-', 'BA',if(laporan = 0, 'BI', if(asesorSatu <> '-' and asesorDua <> '-',if(laporanAsesor1 >= laporan and laporanAsesor2 >= laporan,'SE','BS'),\n" +
            "if(asesorSatu = '-', if(laporanAsesor2 >= laporan,'SE','BS'),if(laporanAsesor1 >= laporan,'SE','BS'))))) as status from\n" +
            "(select a.id,nama,perguruanTinggi,asesorSatu,asesorDua,coalesce(laporan,0) as laporan,coalesce(status_asesor_1,0) laporanAsesor1,coalesce(status_asesor_2,0) laporanAsesor2 from\n" +
            "(select a.id,a.nama_lengkap as nama,d.nama_perguruan_tinggi as perguruanTinggi,coalesce(b.nama_lengkap,'-') as asesorSatu,coalesce(c.nama_lengkap,'-') as asesorDua from dosen as a\n" +
            "inner join perguruan_tinggi as d on a.id_perguruan_tinggi = d.id\n" +
            "left join s_user as b on a.id_asesor_satu = b.id\n" +
            "left join s_user as c on a.id_asesor_dua = c.id where a.status = 'AKTIF' and d.nama_perguruan_tinggi like %?2% or a.nama_lengkap like %?2% order by perguruanTinggi, nama) as a\n" +
            "left join\n" +
            "(select id_dosen,count(id)as laporan from laporan_bkd where status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as b on a.id = b.id_dosen \n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_1 from laporan_bkd where sks_terpenuhi_asesor1 is not null and status = 'AKTIF' and id_semester = '137fdf29-15cd-4338-839a-d20aea1dab82' group by id_dosen\n" +
            ") as c on a.id = c.id_dosen\n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_2 from laporan_bkd where sks_terpenuhi_asesor2 is not null and status = 'AKTIF' and id_semester = '137fdf29-15cd-4338-839a-d20aea1dab82' group by id_dosen\n" +
            ") as d on a.id = d.id_dosen)as a", nativeQuery = true)
    List<PelaporanBkdDto> pelaporanBkdCari(String Semester, String nama);

    @Query(value = "select * from (select id,nama,perguruanTinggi,asesorSatu,asesorDua,\n" +
            "if(asesorSatu = '-' and asesordua = '-', 'BA',if(laporan = 0, 'BI', if(asesorSatu <> '-' and asesorDua <> '-',if(laporanAsesor1 >= laporan and laporanAsesor2 >= laporan,'SE','BS'),\n" +
            "if(asesorSatu = '-', if(laporanAsesor2 >= laporan,'SE','BS'),if(laporanAsesor1 >= laporan,'SE','BS'))))) as status from\n" +
            "(select a.id,nama,perguruanTinggi,asesorSatu,asesorDua,coalesce(laporan,0) as laporan,coalesce(status_asesor_1,0) laporanAsesor1,coalesce(status_asesor_2,0) laporanAsesor2 from\n" +
            "(select a.id,a.nama_lengkap as nama,d.nama_perguruan_tinggi as perguruanTinggi,coalesce(b.nama_lengkap,'-') as asesorSatu,coalesce(c.nama_lengkap,'-') as asesorDua from dosen as a\n" +
            "inner join perguruan_tinggi as d on a.id_perguruan_tinggi = d.id\n" +
            "left join s_user as b on a.id_asesor_satu = b.id\n" +
            "left join s_user as c on a.id_asesor_dua = c.id where a.status = 'AKTIF' and d.nama_perguruan_tinggi like %?2% or a.nama_lengkap like %?2% order by perguruanTinggi, nama) as a\n" +
            "left join\n" +
            "(select id_dosen,count(id)as laporan from laporan_bkd where status = 'AKTIF' and id_semester = ?1 group by id_dosen\n" +
            ") as b on a.id = b.id_dosen \n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_1 from laporan_bkd where sks_terpenuhi_asesor1 is not null and status = 'AKTIF' and id_semester = '137fdf29-15cd-4338-839a-d20aea1dab82' group by id_dosen\n" +
            ") as c on a.id = c.id_dosen\n" +
            "left join\n" +
            "(select id_dosen,count(id)as status_asesor_2 from laporan_bkd where sks_terpenuhi_asesor2 is not null and status = 'AKTIF' and id_semester = '137fdf29-15cd-4338-839a-d20aea1dab82' group by id_dosen\n" +
            ") as d on a.id = d.id_dosen)as a) b where status = ?3", nativeQuery = true)
    List<PelaporanBkdDto> pelaporanBkdCariDenganStatus(String Semester, String nama, String status);


    @Query(value = "select idDosen,namaLengkap as namaDosen,if(diisi = 0, 'BI', if(dinilai = 0, 'BN', if(dinilai < diisi , 'BSN','SN'))) as status from\n" +
            "(select a.*,coalesce(diisi,0)as diisi, coalesce(dinilai,0) as dinilai from\n" +
            "(select id as idDosen, nama_lengkap as namaLengkap from dosen \n" +
            "where status = 'AKTIF' and (id_asesor_satu = ?1 or id_asesor_dua = ?1)) as a\n" +
            "left join\n" +
            "(select count(id) as diisi, id_dosen from laporan_bkd where status = 'AKTIF' and id_semester = ?2) as b on a.idDosen = b.id_dosen\n" +
            "left join\n" +
            "(select count(id) as dinilai, id_dosen from laporan_bkd where (asesor1 is not null or asesor2 is not null) and status = 'AKTIF' \n" +
            "and id_semester = ?2) as c on a.idDosen = c.id_dosen) as a order by namaLengkap", nativeQuery = true)
    List<ListAssesmentDto> listAssesment(String idUser, String semester);

    @Query(value = "select idDosen,namaLengkap as namaDosen,if(diisi = 0, 'BI', if(dinilai = 0, 'BN', if(dinilai < diisi , 'BSN','SN'))) as status from\n" +
            "(select a.*,coalesce(diisi,0)as diisi, coalesce(dinilai,0) as dinilai from\n" +
            "(select id as idDosen, nama_lengkap as namaLengkap from dosen \n" +
            "where status = 'AKTIF' and (id_asesor_satu = ?1 or id_asesor_dua = ?1)) as a\n" +
            "left join\n" +
            "(select count(id) as diisi, id_dosen from laporan_bkd where status = 'AKTIF' and id_semester = ?2) as b on a.idDosen = b.id_dosen\n" +
            "left join\n" +
            "(select count(id) as dinilai, id_dosen from laporan_bkd where (asesor1 is not null or asesor2 is not null) and status = 'AKTIF' \n" +
            "and id_semester = ?2) as c on a.idDosen = c.id_dosen) as a where namaLengkap like %?3% order by namaLengkap", nativeQuery = true)
    List<ListAssesmentDto> listAssesmentSearch(String idUser, String semester, String search);


    @Query(value = "select * from (select idDosen,namaLengkap as namaDosen,if(diisi = 0, 'BI', if(dinilai = 0, 'BN', if(dinilai < diisi , 'BSN','SN'))) as status from\n" +
            "(select a.*,coalesce(diisi,0)as diisi, coalesce(dinilai,0) as dinilai from\n" +
            "(select id as idDosen, nama_lengkap as namaLengkap from dosen \n" +
            "where status = 'AKTIF' and (id_asesor_satu = ?1 or id_asesor_dua = ?1)) as a\n" +
            "left join\n" +
            "(select count(id) as diisi, id_dosen from laporan_bkd where status = 'AKTIF' and id_semester = ?2) as b on a.idDosen = b.id_dosen\n" +
            "left join\n" +
            "(select count(id) as dinilai, id_dosen from laporan_bkd where (asesor1 is not null or asesor2 is not null) and status = 'AKTIF' \n" +
            "and id_semester = ?2) as c on a.idDosen = c.id_dosen) as a) as a where status = ?3 order by namaDosen", nativeQuery = true)
    List<ListAssesmentDto> listAssesmentStatus(String idUser, String semester, String status);


    @Query(value = "select * from (select idDosen,namaLengkap as namaDosen,if(diisi = 0, 'BI', if(dinilai = 0, 'BN', if(dinilai < diisi , 'BSN','SN'))) as status from\n" +
            "(select a.*,coalesce(diisi,0)as diisi, coalesce(dinilai,0) as dinilai from\n" +
            "(select id as idDosen, nama_lengkap as namaLengkap from dosen \n" +
            "where status = 'AKTIF' and (id_asesor_satu = ?1 or id_asesor_dua = ?1)) as a\n" +
            "left join\n" +
            "(select count(id) as diisi, id_dosen from laporan_bkd where status = 'AKTIF' and id_semester = ?2) as b on a.idDosen = b.id_dosen\n" +
            "left join\n" +
            "(select count(id) as dinilai, id_dosen from laporan_bkd where (asesor1 is not null or asesor2 is not null) and status = 'AKTIF' \n" +
            "and id_semester = ?2) as c on a.idDosen = c.id_dosen) as a) as a where status = ?3 and namaDosen like %?4% order by namaDosen", nativeQuery = true)
    List<ListAssesmentDto> listAssesmentStatusSearch(String idUser, String semester, String status, String search);

    @Query("select lb from LaporanBkd lb where lb.sksTerpenuhiAsesor1 is null and lb.status = ?1 and lb.semester = ?2")
    List<LaporanBkd> laporanBkdBelumAsesorSatu(StatusRecord statusRecord, Semester semester);

    @Query("select lb from LaporanBkd lb where lb.sksTerpenuhiAsesor2 is null and lb.status = ?1 and lb.semester = ?2")
    List<LaporanBkd> laporanBkdBelumAsesorDua(StatusRecord statusRecord, Semester semester);
    
}

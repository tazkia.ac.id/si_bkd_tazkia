package id.tazkia.sibkd.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.JabatanStruktural;
import id.tazkia.sibkd.entity.PerguruanTinggi;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;


public interface DosenDao extends PagingAndSortingRepository<Dosen, String>, CrudRepository<Dosen, String> {

    Page<Dosen> findByStatusAndPerguruanTinggiAndNamaContainingIgnoreCaseOrStatusAndPerguruanTinggiAndEmailContainingIgnoreCase(StatusRecord statusRecord, PerguruanTinggi perguruanTinggi, String nama, StatusRecord statusRecord2, PerguruanTinggi perguruanTinggi2, String email, Pageable pageable);
    Page<Dosen> findByStatusAndNamaContainingIgnoreCaseOrStatusAndEmailContainingIgnoreCase(StatusRecord statusRecord, String nama, StatusRecord statusRecord1, String Email, Pageable pageable);
    Page<Dosen> findByStatusAndJabatanStrukturalAndNamaContainingIgnoreCaseOrStatusAndJabatanStrukturalAndEmailContainingIgnoreCase(StatusRecord statusRecord, JabatanStruktural jabatanStruktural, String nama, StatusRecord statusRecord1,JabatanStruktural jabatanStruktural1, String Email, Pageable pageable);
    Page<Dosen> findByStatusAndPerguruanTinggiAndJabatanStrukturalAndNamaContainingIgnoreCaseOrStatusAndPerguruanTinggiAndJabatanStrukturalAndEmailContainingIgnoreCase(StatusRecord statusRecord, PerguruanTinggi perguruanTinggi, JabatanStruktural jabatanStruktural, String nama, StatusRecord statusRecord1, PerguruanTinggi perguruanTinggi2,JabatanStruktural jabatanStruktural1, String Email, Pageable pageable);

    Page<Dosen> findByStatusOrderByNama(StatusRecord statusRecord, Pageable pageable);

    Integer countByStatusAndIdNotAndEmail(StatusRecord statusRecord, String id, String email);

    Page<Dosen> findByStatusAndNamaContainingIgnoreCaseOrStatusAndEmailContainingIgnoreCaseOrStatusAndPerguruanTinggiNamaPerguruanTinggiContainingIgnoreCaseOrderByNama(StatusRecord statusRecord, String nama, StatusRecord statusRecord2, String email, StatusRecord statusRecord3, String perguruanTinggi, Pageable page);

//    Page<Dosen> findByStatusAndPerguruanTinggiOrderByNama(StatusRecord statusRecord, PerguruanTinggi perguruanTinggi, Pageable pageable);

    Page<Dosen> findByStatusAndPerguruanTinggiOrderByNama(StatusRecord statusRecord, PerguruanTinggi perguruanTinggi, Pageable pageable);

    Page<Dosen> findByStatusAndPerguruanTinggiAndNamaContainingIgnoreCaseOrderByNama(StatusRecord statusRecord, PerguruanTinggi perguruanTinggi, String nama, Pageable pageable);
    Dosen findByUser(User user);

    Page<Dosen> findByStatusAndNamaContainingIgnoreCaseOrderByNama(StatusRecord statusRecord, String nama, Pageable pageable);




}

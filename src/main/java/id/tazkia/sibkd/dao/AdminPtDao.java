package id.tazkia.sibkd.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.AdminPt;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;

public interface AdminPtDao extends PagingAndSortingRepository<AdminPt, String>, CrudRepository<AdminPt, String> {
    Page<AdminPt> findByStatusOrderByNama(StatusRecord statusRecord, Pageable pageable);
    Page<AdminPt> findByStatusNotInOrderByNama(List<StatusRecord> statusRecord, Pageable pageable);
    AdminPt findByUser(User user);
    Page<AdminPt> findByNamaContainingIgnoreCaseAndStatusOrEmailContainingIgnoreCaseAndStatusOrPerguruanTinggiNamaPerguruanTinggiContainingIgnoreCaseAndStatus(String nama, StatusRecord statusRecord1, String email, StatusRecord statusRecord2, String pt, StatusRecord statusRecord, Pageable pageable);
    Page<AdminPt> findByNamaContainingIgnoreCaseAndStatusNotInOrEmailContainingIgnoreCaseAndStatusNotInOrPerguruanTinggiNamaPerguruanTinggiContainingIgnoreCaseAndStatusNotIn(String nama, List<StatusRecord> statusRecord1, String email, List<StatusRecord> statusRecord2, String pt, List<StatusRecord> statusRecord, Pageable pageable);
}

package id.tazkia.sibkd.dao;

import id.tazkia.sibkd.entity.AssesmentAll;
import id.tazkia.sibkd.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AssesmentAllDao extends PagingAndSortingRepository<AssesmentAll, String>, CrudRepository<AssesmentAll, String> {

    Page<AssesmentAll> findByStatusOrderByTanggalDesc(StatusRecord statusRecord, Pageable pageable);

    AssesmentAll findFirstByStatusAndStatusAssesmentOrderByTanggal(StatusRecord statusRecord, StatusRecord statusRecord2);

}

package id.tazkia.sibkd.dao;

import id.tazkia.sibkd.entity.PerguruanTinggi;
import id.tazkia.sibkd.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PerguruanTinggiDao extends PagingAndSortingRepository<PerguruanTinggi, String>, CrudRepository<PerguruanTinggi, String> {

    List<PerguruanTinggi> findByStatusOrderByNamaPerguruanTinggi(StatusRecord statusRecord);

    List<PerguruanTinggi> findByStatus(StatusRecord statusRecord);
    List<PerguruanTinggi> findByNamaPerguruanTinggiContainingIgnoreCaseAndStatusOrAlamatContainingIgnoreCaseAndStatus( String nama,StatusRecord statusRecord,String alamat, StatusRecord statusRecord2);

    Page<PerguruanTinggi> findByStatusOrderByNamaPerguruanTinggi(StatusRecord statusRecord, Pageable pageable);

    Page<PerguruanTinggi> findByStatusNotInOrderByNamaPerguruanTinggi(List<StatusRecord> statusRecord, Pageable pageable);

    Page<PerguruanTinggi> findByNamaPerguruanTinggiContainingIgnoreCaseAndStatusOrAlamatContainingIgnoreCaseAndStatus( String nama,StatusRecord statusRecord,String alamat, StatusRecord statusRecord2, Pageable pageable);
    Page<PerguruanTinggi> findByNamaPerguruanTinggiContainingIgnoreCaseAndStatusNotInOrAlamatContainingIgnoreCaseAndStatusNotIn( String nama,List<StatusRecord> statusRecord,String alamat, List<StatusRecord> statusRecord2, Pageable pageable);


}

package id.tazkia.sibkd.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.DosenDetail;
import id.tazkia.sibkd.entity.StatusRecord;

public interface DosenDetailDao extends PagingAndSortingRepository<DosenDetail,String>,CrudRepository<DosenDetail, String> {
    DosenDetail findByDosen(Dosen dosen);

    @Query(value = "SELECT * FROM dosen_detail where (foto is null or kartu_nidn is null or nidn is null or nomor_sertifikat is null or sertifikat_dosen is null or fakultas is null or prodi is null or alamat is null or sk_dt is null or jabatan_fungsional is null or sk_jafung is null or judul_skripsi is null or ijazahs1 is null or perguruans1 is null or judul_tesis is null or ijazahs2 is null or perguruans2 is null or judul_disertasi is null or ijazahs3 is null or perguruans3 is null) and id_dosen = '0be6f482-b043-41a4-804d-a53d3026e930'", nativeQuery = true)
    DosenDetail validasiKosong(Dosen id);

    DosenDetail findByStatusAndDosen(StatusRecord statusRecord, Dosen dosen);
}

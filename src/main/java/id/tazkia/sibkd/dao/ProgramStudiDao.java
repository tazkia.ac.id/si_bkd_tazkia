package id.tazkia.sibkd.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.ProgramStudi;
import id.tazkia.sibkd.entity.StatusRecord;

public interface ProgramStudiDao extends PagingAndSortingRepository<ProgramStudi, String>, CrudRepository<ProgramStudi, String> {

    ProgramStudi findByStatusAndId(StatusRecord statusRecord, String id);
    List<ProgramStudi> findByStatus(StatusRecord statusRecord);
    Page<ProgramStudi> findByStatusOrderByNamaProgramStudi(StatusRecord statusRecord, Pageable pageable);
    List<ProgramStudi> findByStatusOrderByNamaProgramStudi(StatusRecord statusRecord);
    Page<ProgramStudi> findByStatusAndNamaProgramStudiContainingIgnoreCaseOrderByNamaProgramStudi(StatusRecord statusRecord, String nama, Pageable pageable);


}

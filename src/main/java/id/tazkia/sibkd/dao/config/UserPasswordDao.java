package id.tazkia.sibkd.dao.config;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.entity.config.UserPassword;

public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String>, CrudRepository<UserPassword, String> {
    UserPassword findByUser(User user);
}

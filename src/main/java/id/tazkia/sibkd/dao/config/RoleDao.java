package id.tazkia.sibkd.dao.config;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.config.Role;

public interface RoleDao extends PagingAndSortingRepository<Role, String>, CrudRepository<Role, String> {


    List<Role> findByJenis(String jenis);

}

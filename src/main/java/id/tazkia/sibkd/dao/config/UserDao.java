package id.tazkia.sibkd.dao.config;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.config.Role;
import id.tazkia.sibkd.entity.config.User;

public interface UserDao extends PagingAndSortingRepository<User, String>, CrudRepository<User, String> {
    User findByUsername(String username);

    User findByUsernameAndIdNot(String username, String id);

    List<User> findByRoleJenis(String jenis);

    Page<User> findByRoleJenis(String jenis, Pageable pageable);

    Page<User> findByRoleJenisAndUsernameNot(String jenis, String usernamem, Pageable pageable);



    Page<User> findByRoleJenisAndUsernameContainingIgnoreCaseOrRoleJenisAndNamaLengkapContainingIgnoreCaseOrderByNamaLengkap(String jenis, String username, String jenis2, String nama, Pageable pageable);

    Page<User> findByRoleJenisAndUsernameNotAndUsernameContainingIgnoreCaseOrRoleJenisAndUsernameNotAndNamaLengkapContainingIgnoreCaseOrderByNamaLengkap(String jenis, String usernames, String username, String jenis2, String usernamess, String nama, Pageable pageable);

    Page<User> findByRole(Role role, Pageable pageable);

    Page<User> findByRoleAndUsernameNotOrderByNamaLengkap(Role role, String username, Pageable pageable);

    List<User> findByRoleOrderByNamaLengkap(Role role);

    List<User> findByRoleNameOrderByNamaLengkap(String role);

    Page<User> findByRoleAndUsernameContainingIgnoreCaseOrRoleAndNamaLengkapContainingIgnoreCaseOrderByNamaLengkap(Role role, String username, Role role2, String nama, Pageable pageable);

    Page<User> findByRoleAndUsernameNotAndUsernameContainingIgnoreCaseOrRoleAndUsernameNotAndNamaLengkapContainingIgnoreCaseOrderByNamaLengkap(Role role,String usernames, String username, Role role2, String usernamess, String nama, Pageable pageable);

}

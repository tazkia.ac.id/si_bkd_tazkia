package id.tazkia.sibkd.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.tazkia.sibkd.entity.LaporanBkd;
import id.tazkia.sibkd.entity.LaporanBkdFile;
import id.tazkia.sibkd.entity.StatusRecord;

public interface LaporanBkdFileDao extends PagingAndSortingRepository<LaporanBkdFile, String>, CrudRepository<LaporanBkdFile, String> {

    List<LaporanBkdFile> findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord statusRecord, List<LaporanBkd> laporanBkds);


    List<LaporanBkdFile> findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsertAscJenisFileLaporanDesc(StatusRecord statusRecord, List<LaporanBkd> laporanBkds);


//    List<LaporanBkdFile> findByStatusAndLaporanBkdOrderByLaporanBkdTanggalInsert(StatusRecord statusRecord, LaporanBkd laporanBkds);

    List<LaporanBkdFile> findByStatusAndLaporanBkdOrderByLaporanBkdTanggalInsert(StatusRecord statusRecord, LaporanBkd laporanBkd);

}

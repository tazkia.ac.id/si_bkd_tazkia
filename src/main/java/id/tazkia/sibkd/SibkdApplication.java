package id.tazkia.sibkd;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import jakarta.annotation.PostConstruct;
import nz.net.ultraq.thymeleaf.LayoutDialect;

@SpringBootApplication
public class SibkdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SibkdApplication.class, args);
	}

	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

//	@Bean
//	public MustacheFactory mustacheFactory(){
//		return new DefaultMustacheFactory();
//	}

	@PostConstruct
	void setUTCTimeZone(){
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
}

package id.tazkia.sibkd.controller;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.DosenDao;
import id.tazkia.sibkd.dao.DosenDetailDao;
import id.tazkia.sibkd.dao.JabatanStrukturalDao;
import id.tazkia.sibkd.dao.PerguruanTinggiDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.dto.BaseResponse;
import id.tazkia.sibkd.dto.BiodataDto;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.DosenDetail;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class SettingDosenController {
    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private JabatanStrukturalDao jabatanStrukturalDao;

    @Autowired
    private PerguruanTinggiDao perguruanTinggiDao;

    @Autowired
    private DosenDetailDao dosenDetailDao;
    @Autowired
    private UserDao userDao;

    @GetMapping("/setting_dosen")
    public String getSettingDosen(Model model,
                                  @RequestParam (required = false) String search,
                                  @PageableDefault Pageable pageable){

        model.addAttribute("menusetting","active");
        model.addAttribute("menusettingDosen", "active");

        if(search != null){
            model.addAttribute("search", search);
            model.addAttribute("dosen", dosenDao.findByStatusAndNamaContainingIgnoreCaseOrStatusAndEmailContainingIgnoreCaseOrStatusAndPerguruanTinggiNamaPerguruanTinggiContainingIgnoreCaseOrderByNama(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, StatusRecord.AKTIF, search , pageable));
        }else {
            model.addAttribute("dosen", dosenDao.findByStatusOrderByNama(StatusRecord.AKTIF, pageable));
        }

        return "setting_dosen/list";
    }

    @GetMapping("/setting_dosen/form")
    public String formSettingDosen(Model model, @RequestParam(required = false) String id){
        model.addAttribute("dosen", new Dosen());
        if (id != null && !id.isEmpty()){
            Dosen dosen = dosenDao.findById(id).get();
            if (dosen != null){
                model.addAttribute("dosen", dosen);
            }
        }
        model.addAttribute("menusetting","active");
        model.addAttribute("menusettingDosen", "active");
        model.addAttribute("perguruanTinggi", perguruanTinggiDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("listJabatan", jabatanStrukturalDao.findByStatus(StatusRecord.AKTIF));
        return "setting_dosen/form";

    }

    @PostMapping("/setting_dosen/save")
    public String saveSettingDosen(@ModelAttribute @Valid Dosen dosen, Authentication authentication, Model model, RedirectAttributes attributes){
        User currentUser = currentUserService.currentUser(authentication);

        Integer ada = dosenDao.countByStatusAndIdNotAndEmail(StatusRecord.AKTIF, dosen.getId(), dosen.getEmail());

        if(ada > 0){
            model.addAttribute("dosen", dosen);
            model.addAttribute("ada", "email sudah terdaftar, silahkan cek pada list dosen");
            return "setting_dosen/form";
        }

        if (dosen.getUser() == null) {
            User user = currentUserService.createUser(dosen.getNama(), dosen.getEmail(), "dosen");
            dosen.setUser(user);
        }else{
            User user = dosen.getUser();
            user.setUsername(dosen.getEmail());
        }

        dosen.setStatus(StatusRecord.AKTIF);
        dosenDao.save(dosen);

        attributes.addFlashAttribute("success", "Data berhasil disimpan..");
        return "redirect:/setting_dosen";
    }

    @GetMapping("/setting_dosen/detail")
    public String settingDosenDetail(Model model,
                                     @RequestParam(required = true) Dosen dosen){

        DosenDetail dosenDetail = dosenDetailDao.findByDosen(dosen);

        if (dosenDetail != null){
            BiodataDto biodataDto = new BiodataDto();
            biodataDto.setId(dosenDetail.getId());
            biodataDto.setIdDosen(dosen.getId());
            biodataDto.setNama(dosen.getNama());
            biodataDto.setProfil(dosenDetail.getFoto());
            biodataDto.setEmail(dosen.getEmail());
            biodataDto.setTelepon(dosen.getTelepon());
            biodataDto.setNamaProdi(dosenDetail.getProdi().getNamaProgramStudi());
            biodataDto.setJabatanStruktural(dosen.getJabatanStruktural());
            biodataDto.setPerguruanTinggi(dosen.getPerguruanTinggi());
            BeanUtils.copyProperties(dosenDetail,biodataDto);
            model.addAttribute("biodata", biodataDto);

        }else {
            BiodataDto biodataDto = new BiodataDto();
            biodataDto.setIdDosen(dosen.getId());
            biodataDto.setNama(dosen.getNama());
            biodataDto.setEmail(dosen.getEmail());
            biodataDto.setTelepon(dosen.getTelepon());
            biodataDto.setPerguruanTinggi(dosen.getPerguruanTinggi());
            biodataDto.setJabatanStruktural(dosen.getJabatanStruktural());
            model.addAttribute("biodata", biodataDto);
        }
        return "setting_dosen/detail";
    }


    @PostMapping("/setting_dosen/delete")
    public String deleteSettingDosen(@ModelAttribute @Valid Dosen dosen, Authentication authentication, Model model, RedirectAttributes attributes){
        User currentUser = currentUserService.currentUser(authentication);

        dosen.setStatus(StatusRecord.HAPUS);

        User user = dosen.getUser();

        user.setActive(false);
        user.setUsername(user.getUsername()+"-HAPUS");

        userDao.save(user);
        dosenDao.save(dosen);
        attributes.addFlashAttribute("success", "Data berhasil dihapus..");
        return "redirect:/setting_dosen";

    }


    @GetMapping("/cekEmailDosen")
    @ResponseBody
    public BaseResponse cekEmail(@RequestParam String email, @RequestParam (required = false) String id){

        System.out.println("email : "+ email);
        System.out.println("id : " + id);

        
        if(id.isEmpty()){
            User user = userDao.findByUsername(email);
            if (user != null){
                BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.FORBIDDEN).message("Telah Digunakan").build();
                return baseResponse;
            }else {
                BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.NOT_FOUND).message("Belum Digunakan").build();
                return baseResponse;
            }
        }else{
            Dosen dosen = dosenDao.findById(id).get();
            System.out.println("id : " + dosen.getUser().getId());
            User user = userDao.findByUsernameAndIdNot(email, dosen.getUser().getId());
            if (user != null){
                BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.FORBIDDEN).message("Telah Digunakan").build();
                return baseResponse;
            }else {
                BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.NOT_FOUND).message("Belum Digunakan").build();
                return baseResponse;
            }
        }
        
    }


}

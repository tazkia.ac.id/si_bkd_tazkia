package id.tazkia.sibkd.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.AdminPtDao;
import id.tazkia.sibkd.dao.PerguruanTinggiDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.dto.BaseResponse;
import id.tazkia.sibkd.entity.AdminPt;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class AdminPerguruanTinggiController {

    @Autowired
    private PerguruanTinggiDao perguruanTinggiDao;
    @Autowired
    private AdminPtDao adminPtDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    CurrentUserService currentUserService;

    @GetMapping("/admin_perguruan_tinggi")
    public String adminPerguruanTinggi(Model model, @RequestParam(required = false) String search,@PageableDefault(size = 20) Pageable pageable){
        if (search == null){
            model.addAttribute("list", adminPtDao.findByStatusNotInOrderByNama(Arrays.asList(StatusRecord.HAPUS), pageable));
        }else {
            model.addAttribute("list", adminPtDao.findByNamaContainingIgnoreCaseAndStatusNotInOrEmailContainingIgnoreCaseAndStatusNotInOrPerguruanTinggiNamaPerguruanTinggiContainingIgnoreCaseAndStatusNotIn(search,Arrays.asList(StatusRecord.HAPUS),search, Arrays.asList(StatusRecord.HAPUS),search, Arrays.asList(StatusRecord.HAPUS),pageable));
            model.addAttribute("search", search);
        }


        model.addAttribute("menusetting","active");
        model.addAttribute("menuadminperguruantinggi", "active");

        return "admin_perguruan_tinggi/list";
    }

    @GetMapping("/admin_perguruan_tinggi/baru")
    public String adminPerguruanTinggiBaru(Model model,@RequestParam(required = false) String id){
        model.addAttribute("adminPt", new AdminPt());

        if (id != null && !id.isEmpty()){
            AdminPt adminPt = adminPtDao.findById(id).get();
            if (adminPt != null){
                model.addAttribute("adminPt", adminPt);
            }
        }

        model.addAttribute("menusetting","active");
        model.addAttribute("menuadminperguruantinggi", "active");
        model.addAttribute("perguruan", perguruanTinggiDao.findByStatusOrderByNamaPerguruanTinggi(StatusRecord.AKTIF));
        return "admin_perguruan_tinggi/form";
    }

    @GetMapping("/admin_perguruan_tinggi/edit")
    public String adminPerguruanTinggiEdit(Model model){

        model.addAttribute("menusetting","active");
        model.addAttribute("menuadminperguruantinggi", "active");
        return "admin_perguruan_tinggi/form";
    }

    @PostMapping("/admin_perguruan_tinggi/simpan")
    public String postBiodata(@ModelAttribute @Valid AdminPt adminPt, RedirectAttributes attributes){
//        User user = currentUserService.createUser(adminPt.getNama(),adminPt.getEmail(),"adminpt");

        if (adminPt.getUser() == null) {
            User user = currentUserService.createUser(adminPt.getNama(),adminPt.getEmail(),"adminpt");
            adminPt.setUser(user);
        }else {
            User user = currentUserService.updateUser(adminPt.getUser(),adminPt.getEmail(),adminPt.getNama());
        }

        adminPtDao.save(adminPt);

        return "redirect:../admin_perguruan_tinggi";
    }

    @GetMapping("/cekEmail")
    @ResponseBody
    public BaseResponse cekEmail(@RequestParam String email){
        User user = userDao.findByUsername(email);

        if (user != null){
            BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.FORBIDDEN).message("Telah Digunakan").build();
            return baseResponse;
        }else {
            BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.NOT_FOUND).message("Belum Digunakan").build();
            return baseResponse;
        }
    }

    



}

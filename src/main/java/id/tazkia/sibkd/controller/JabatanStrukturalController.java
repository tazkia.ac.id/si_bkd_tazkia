package id.tazkia.sibkd.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.JabatanStrukturanDao;
import id.tazkia.sibkd.entity.JabatanStruktural;
import id.tazkia.sibkd.entity.StatusRecord;
import jakarta.validation.Valid;

@Controller
public class JabatanStrukturalController {

    @Autowired
    private JabatanStrukturanDao jabatanStrukturanDao;

    @GetMapping("/jabatan_struktural")
    public String jabatanStruktural(Model model, @PageableDefault(size = 20)Pageable pageable){

        model.addAttribute("menujabatanstruktural","active");
        model.addAttribute("list", jabatanStrukturanDao.findByStatus(StatusRecord.AKTIF, pageable));
        return "jabatan_struktural/list";
    }

    @GetMapping("/jabatan_struktural/baru")
    public String jabatanStrukturalBaru(Model model, RedirectAttributes attributes){

        model.addAttribute("jabatan", new JabatanStruktural());
        model.addAttribute("menujabatanstruktural","active");
        attributes.addFlashAttribute("success", "Data jabatan struktural berhasil disimpan.");
        return "jabatan_struktural/form";
    }

    @GetMapping("/jabatan_struktural/edit")
    public String jabatanEdit(Model model, @RequestParam(required = false)JabatanStruktural jabatanStruktural){
        model.addAttribute("jabatan", jabatanStruktural);

        return "jabatan_struktural/form";
    }

    @PostMapping("/jabatanStruktural/baru")
    public String jabatanBaru(@Valid JabatanStruktural jabatanStruktural){
        jabatanStruktural.setStatus(StatusRecord.AKTIF);
        jabatanStrukturanDao.save(jabatanStruktural);
        return "redirect:/jabatan_struktural";
    }


}

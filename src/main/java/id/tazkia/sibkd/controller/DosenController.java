package id.tazkia.sibkd.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import id.tazkia.sibkd.dao.AdminPtDao;
import id.tazkia.sibkd.dao.DosenDao;
import id.tazkia.sibkd.dao.JabatanStrukturalDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.dto.BaseResponse;
import id.tazkia.sibkd.entity.AdminPt;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.JabatanStruktural;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class DosenController {
    @Autowired
    private DosenDao dosenDao;
    @Autowired
    private JabatanStrukturalDao jabatanStrukturalDao;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private AdminPtDao adminPtDao;
    @Autowired
    private UserDao userDao;

    @GetMapping("/dosen")
    public String dosen(Model model,@RequestParam(required = false) String search,
                        @RequestParam(required = false) String jabatan,@PageableDefault(size = 20) Pageable pageable,
                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        AdminPt adminPt = adminPtDao.findByUser(user);

        if (search == null){
            model.addAttribute("list", dosenDao.findByStatusAndPerguruanTinggiOrderByNama(StatusRecord.AKTIF, adminPt.getPerguruanTinggi(),pageable));
        }else {
            if (jabatan.equals("-")){
                model.addAttribute("list", dosenDao.findByStatusAndPerguruanTinggiAndNamaContainingIgnoreCaseOrStatusAndPerguruanTinggiAndEmailContainingIgnoreCase(StatusRecord.AKTIF,adminPt.getPerguruanTinggi(),search,StatusRecord.AKTIF, adminPt.getPerguruanTinggi(),search,pageable));
            }else {
                JabatanStruktural jabatanStruktural = jabatanStrukturalDao.findById(jabatan).get();
                model.addAttribute("list", dosenDao.findByStatusAndPerguruanTinggiAndJabatanStrukturalAndNamaContainingIgnoreCaseOrStatusAndPerguruanTinggiAndJabatanStrukturalAndEmailContainingIgnoreCase(StatusRecord.AKTIF,adminPt.getPerguruanTinggi(),jabatanStruktural,search,StatusRecord.AKTIF,adminPt.getPerguruanTinggi(),jabatanStruktural,search,pageable));
                model.addAttribute("selectedJabatan", jabatanStruktural);
            }
            model.addAttribute("search", search);

        }

        model.addAttribute("menusetting","active");
        model.addAttribute("menudosen", "active");
        model.addAttribute("listJabatan", jabatanStrukturalDao.findByStatus(StatusRecord.AKTIF));

        return "dosen/list";
    }

    @GetMapping("/dosen/baru")
    public String dosenBaru(Model model, @RequestParam(required = false) String id){

        model.addAttribute("dosen", new Dosen());

        if (id != null && !id.isEmpty()){
            Dosen dosen = dosenDao.findById(id).get();
            if (dosen != null){
                model.addAttribute("dosen", dosen);
            }
        }

        model.addAttribute("listJabatan", jabatanStrukturalDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("menusetting","active");
        model.addAttribute("menudosen", "active");
        return "dosen/form";
    }

    @PostMapping("/dosen/simpan")
    public String postDosen(Authentication authentication, @ModelAttribute @Valid Dosen dosen){
        User currentUser = currentUserService.currentUser(authentication);
        AdminPt adminPt = adminPtDao.findByUser(currentUser);

        if (dosen.getUser() == null) {
            User user = currentUserService.createUser(dosen.getNama(), dosen.getEmail(), "dosen");
            dosen.setUser(user);
        }else {
            User user = currentUserService.updateUser(dosen.getUser(),dosen.getEmail(),dosen.getNama());
        }

        // if(currentUser.getRole().getName().equals("ADMINPT")){
            dosen.setPerguruanTinggi(adminPt.getPerguruanTinggi());
        // }
        
        dosen.setStatus(StatusRecord.AKTIF);
        dosenDao.save(dosen);

        return "redirect:../dosen";
    }

    @GetMapping("/dosen/update-asessor1/{id}")
    @ResponseBody
    public BaseResponse updateAsessor1(@PathVariable String id, @RequestParam String idAsessor){
        if (idAsessor.equals("-")) {
            Dosen dosen = dosenDao.findById(id).get();
            dosen.setAsesorSatu(null);
            dosen.setAsesorDua(null);
            dosenDao.save(dosen);

        } else{
            Dosen dosen = dosenDao.findById(id).get();
            if (dosen.getAsesorDua() != null) {
                if (idAsessor.equals(dosen.getAsesorDua().getId())) {
                    if (dosen.getAsesorSatu() == null) {
                        BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.CONFLICT).message("-").build();
                        return baseResponse;
                    } else {
                        BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.CONFLICT).message(dosen.getAsesorSatu().getId()).build();
                        return baseResponse;
                    }
                } else {
                    dosen.setAsesorSatu(userDao.findById(idAsessor).get());
                    dosenDao.save(dosen);
                }
            }else {
                dosen.setAsesorSatu(userDao.findById(idAsessor).get());
                dosenDao.save(dosen);
            }
        }

        BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.OK).message("Berhasil disimpan").build();
        return baseResponse;

    }

    @GetMapping("/dosen/reset-asessor/{id}")
    @ResponseBody
    public BaseResponse reset(@PathVariable String id){
        Dosen dosen = dosenDao.findById(id).get();
        dosen.setAsesorSatu(null);
        dosen.setAsesorDua(null);
        dosenDao.save(dosen);



        BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.OK).message("Berhasil disimpan").build();
        return baseResponse;

    }

    @GetMapping("/dosen/update-asessor2/{id}")
    @ResponseBody
    public BaseResponse updateAsessor2(@PathVariable String id, @RequestParam String idAsessor) {
        if (idAsessor.equals("-")) {
            Dosen dosen = dosenDao.findById(id).get();
            dosen.setAsesorDua(null);
            dosenDao.save(dosen);

        } else{
            Dosen dosen = dosenDao.findById(id).get();
            if (idAsessor.equals(dosen.getAsesorSatu().getId())){
                if (dosen.getAsesorDua() == null){
                    BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.CONFLICT).message("-").build();
                    return baseResponse;
                }else {
                    BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.CONFLICT).message(dosen.getAsesorDua().getId()).build();
                    return baseResponse;
                }
            }else {
                dosen.setAsesorDua(userDao.findById(idAsessor).get());
                dosenDao.save(dosen);
            }
        }

        BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.OK).message("Berhasil disimpan").build();
        return baseResponse;

    }







    @PostMapping("/dosen/update-asesor-dosen")
    @ResponseBody
    public BaseResponse updateAsessorDosen(@RequestParam(required = true) String dosen, @RequestParam(required = false) String idAsessorSatu, @RequestParam(required = false) String idAsesorDua) {
        Dosen dosen2 = dosenDao.findById(dosen).get();
        System.out.println("Proses save asesor dimulai");
        
        if (idAsessorSatu == null && idAsesorDua == null) {
            dosen2.setAsesorSatu(null);
            dosen2.setAsesorDua(null);
        } else {
            if (idAsessorSatu == null) {
                dosen2.setAsesorSatu(null);
                dosen2.setAsesorDua(userDao.findById(idAsesorDua).orElse(null));
            } else if (idAsesorDua == null) {
                dosen2.setAsesorSatu(userDao.findById(idAsessorSatu).orElse(null));
                dosen2.setAsesorDua(null);
            }
        }
        
        dosenDao.save(dosen2);
        System.out.println("Proses save asesor selesai");
        
        BaseResponse baseResponse = BaseResponse.builder().code(HttpStatus.OK).message("Berhasil disimpan").build();
        return baseResponse;
}
}

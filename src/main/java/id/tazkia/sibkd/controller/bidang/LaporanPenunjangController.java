package id.tazkia.sibkd.controller.bidang;

import id.tazkia.sibkd.dao.*;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.*;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
public class LaporanPenunjangController {

    @Autowired
    private UserDao userDao;
    @Autowired
    private DosenDao dosenDao;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private AdminPtDao adminPtDao;
    @Autowired
    private DosenDetailDao dosenDetailDao;
    @Autowired
    private SemesterDao semesterDao;
    @Autowired
    private KegiatanDao kegiatanDao;

    @Autowired
    private LaporanBkdFileDao laporanBkdFileDao;

    @Autowired
    private LaporanBkdDao laporanBkdDao;

    @Autowired
    @Value("${upload.penugasan}")
    private String uploadPenugasan;

    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;

    @GetMapping("/bidang/penunjang")
    public String bidangPenunjang(Model model,
                                  Authentication authentication,@RequestParam(required = false) Semester selectSemester){

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        DosenDetail dosenDetail = dosenDetailDao.findByDosen(dosen);

        if (dosenDetail == null){
            model.addAttribute("validDosen",dosen);
        }else {
            DosenDetail validDosen = dosenDetailDao.validasiKosong(dosen);
            if (validDosen != null){
                model.addAttribute("validDosen",dosen);

            }

        }

        //        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());
        if(selectSemester != null){
            semester = selectSemester;
        }

        Integer ada = semesterDao.cariJadwalPelaporanAktif(semester.getId());
        List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndBidangAndSemesterOrderByTanggalInsert(StatusRecord.AKTIF, dosen, Bidang.PENUNJANG, semester);


        model.addAttribute("dosen", dosen);
        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("semester", semester);
        model.addAttribute("ada", ada);
        model.addAttribute("listLaporanBkd", laporanBkds);
        model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkds));

        model.addAttribute("menubidang","active");
        model.addAttribute("menubidangpenunjang","active");
        return "bidang/penunjang/list";
    }

    @GetMapping("/bidang/penunjang/tambah")
    public String bidangPendidikanTambah(Model model){

        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidangOrderByNamaKegiatan(StatusRecord.AKTIF, Bidang.PENUNJANG));
        model.addAttribute("laporanBkd",new LaporanBkd());
        model.addAttribute("menubidang","active");
        model.addAttribute("menubidangpendidikan","active");
        return "bidang/penunjang/form";
    }

    @GetMapping("/bidang/penunjang/edit")
    public String bidangPendidikanEdit(@RequestParam(required = true)LaporanBkd laporanBkd,
                                       Model model){

        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidangOrderByNamaKegiatan(StatusRecord.AKTIF, Bidang.PENUNJANG));
        model.addAttribute("laporanBkd", laporanBkd);
        model.addAttribute("menubidang","active");
        model.addAttribute("menubidangpenelitian","active");
        return "bidang/penunjang/edit";

    }

    @PostMapping("/bidang/penunjang/hapus")
    public String bidangPendidikanHapus(@ModelAttribute @Valid LaporanBkd laporanBkd,
                                        Authentication authentication,
                                        RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());

        laporanBkd.setStatus(StatusRecord.HAPUS);
        laporanBkd.setTanggalUpdate(LocalDateTime.now());
        laporanBkdDao.save(laporanBkd);

        redirectAttributes.addFlashAttribute("success","Data berhasil dihapus");
        return "redirect:../penunjang";
    }

    @PostMapping("/bidang/penunjang/simpan")
    public String bidangPenelitainSimpan(@ModelAttribute @Valid LaporanBkd laporanBkd,
                                         @RequestParam("filePenugasan") MultipartFile filePenugasan,
                                         @RequestParam("fileKinerja") MultipartFile fileKinerja,
                                         Authentication authentication,
                                         RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());

        laporanBkd.setBidang(Bidang.PENUNJANG);
        laporanBkd.setDosen(dosen);
        laporanBkd.setLaporan(StatusRecord.AKTIF);
        laporanBkd.setPerbaikan(StatusRecord.NONAKTIF);
        laporanBkd.setStatus(StatusRecord.AKTIF);
        laporanBkd.setJenisKegiatan(laporanBkd.getKegiatan().getNamaKegiatan());
        laporanBkd.setTanggalInsert(LocalDateTime.now());
        laporanBkd.setSemester(semester);

        if(filePenugasan != null) {
            LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
            laporanBkd.getLaporanBkdFiles().add(laporanBkdFile);
            laporanBkdFile.setLaporanBkd(laporanBkd);
            laporanBkdFile.setStatus(StatusRecord.AKTIF);
            String namaAsli = filePenugasan.getOriginalFilename();
            Long ukuran = filePenugasan.getSize();
            String extension = "";
            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            laporanBkdFile.setFile(idFile + "." + extension);
            new File(uploadPenugasan).mkdirs();
            File tujuan = new File(uploadPenugasan + File.separator + idFile + "." + extension);
            filePenugasan.transferTo(tujuan);
            laporanBkdFile.setNamaFile(namaAsli);
            laporanBkdFile.setExtensi(extension);
            laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.PENUGASAN);
            laporanBkdFileDao.save(laporanBkdFile);
        }
        if(fileKinerja != null) {
            LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
            laporanBkd.getLaporanBkdFiles().add(laporanBkdFile);
            laporanBkdFile.setLaporanBkd(laporanBkd);
            laporanBkdFile.setStatus(StatusRecord.AKTIF);
            String namaAsli1 = fileKinerja.getOriginalFilename();
            Long ukuran1 = fileKinerja.getSize();
            String extension1 = "";
            int i = namaAsli1.lastIndexOf('.');
            int p = Math.max(namaAsli1.lastIndexOf('/'), namaAsli1.lastIndexOf('\\'));
            if (i > p) {
                extension1 = namaAsli1.substring(i + 1);
            }
            String idFile1 = UUID.randomUUID().toString();
            laporanBkdFile.setFile(idFile1 + "." + extension1);
            new File(uploadKinerja).mkdirs();
            File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension1);
            fileKinerja.transferTo(tujuan2);
            laporanBkdFile.setNamaFile(namaAsli1);
            laporanBkdFile.setExtensi(extension1);
            laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.KINERJA);
            laporanBkdFileDao.save(laporanBkdFile);
        }


        laporanBkdDao.save(laporanBkd);

        redirectAttributes.addFlashAttribute("success","Data berhasil disimpan");
        return "redirect:../penunjang";
    }

    @PostMapping("/bidang/penunjang/update")
    public String bidangPenelitianUpdate(@ModelAttribute @Valid LaporanBkd laporanBkd,
                                         Authentication authentication,
                                         RedirectAttributes redirectAttributes) {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());

        List<LaporanBkdFile> laporanBkdFiles = laporanBkdFileDao.findByStatusAndLaporanBkdOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF,laporanBkd);


        laporanBkd.setBidang(Bidang.PENUNJANG);
        laporanBkd.setDosen(dosen);
        laporanBkd.setLaporan(StatusRecord.AKTIF);
        laporanBkd.setPerbaikan(StatusRecord.AKTIF);
        laporanBkd.setStatus(StatusRecord.AKTIF);
        laporanBkd.setJenisKegiatan(laporanBkd.getKegiatan().getNamaKegiatan());
        laporanBkd.setTanggalUpdate(LocalDateTime.now());
        laporanBkd.setSemester(semester);



        laporanBkdDao.save(laporanBkd);

        for(LaporanBkdFile a : laporanBkdFiles){

            a.setLaporanBkd(laporanBkd);
            laporanBkdFileDao.save(a);
        }

        redirectAttributes.addFlashAttribute("success","Data berhasil di ubah");
        return "redirect:../penunjang";
    }

    @PostMapping("/bidang/penunjang/tambahfilepenugasan")
    public String bidangPendidikanTambahFilePenugasan(@RequestParam LaporanBkd laporanBkd,
                                                      @RequestParam("filePenugasan") MultipartFile file,
                                                      Authentication authentication,
                                                      RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);

        LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
        laporanBkdFile.setLaporanBkd(laporanBkd);
        laporanBkdFile.setNamaFile(file.getOriginalFilename());
        String namaAsli = file.getOriginalFilename();
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        laporanBkdFile.setFile(idFile + "." + extension);
        new File(uploadPenugasan).mkdirs();
        File tujuan = new File(uploadPenugasan + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);


        laporanBkdFile.setExtensi(extension);
        laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.PENUGASAN);
        laporanBkdFile.setStatus(StatusRecord.AKTIF);
        laporanBkdFileDao.save(laporanBkdFile);

        redirectAttributes.addFlashAttribute("success", "Dokumen penugasan berhasil di tambahkan");
        return"redirect:../penunjang";
    }

    @PostMapping("/bidang/penunjang/tambahfilekinerja")
    public String bidangPendidikanTambahFileKinerja(@RequestParam LaporanBkd laporanBkd,
                                                    @RequestParam("fileKinerja") MultipartFile file,
                                                    Authentication authentication,
                                                    RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);

        LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
        laporanBkdFile.setLaporanBkd(laporanBkd);
        laporanBkdFile.setNamaFile(file.getOriginalFilename());
        String namaAsli = file.getOriginalFilename();
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile1 = UUID.randomUUID().toString();
        laporanBkdFile.setFile(idFile1 + "." + extension);
        new File(uploadKinerja).mkdirs();
        File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);
        file.transferTo(tujuan2);

        laporanBkdFile.setExtensi(extension);
        laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.KINERJA);
        laporanBkdFile.setStatus(StatusRecord.AKTIF);
        laporanBkdFileDao.save(laporanBkdFile);

        redirectAttributes.addFlashAttribute("success", "Dokumen kinerja berhasil di tambahkan");
        return"redirect:../penunjang";
    }

    @GetMapping("/bidang/penunjang/hapus_file")
    public String lappranBkdPendidikanHapusFile(@RequestParam(required = true)LaporanBkdFile laporanBkdFile,
                                                RedirectAttributes redirectAttributes){

        laporanBkdFile.setStatus(StatusRecord.HAPUS);
        laporanBkdFileDao.save(laporanBkdFile);


        redirectAttributes.addFlashAttribute("success", "File berhasil dihapus");
        return"redirect:../penunjang";
    }
}

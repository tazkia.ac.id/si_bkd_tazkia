package id.tazkia.sibkd.controller.bidang;

import id.tazkia.sibkd.dao.*;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.*;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
public class LaporanPendidikanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LaporanPendidikanController.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private DosenDao dosenDao;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private AdminPtDao adminPtDao;
    @Autowired
    private DosenDetailDao dosenDetailDao;
    @Autowired
    private SemesterDao semesterDao;
    @Autowired
    private KegiatanDao kegiatanDao;

    @Autowired
    private LaporanBkdDao laporanBkdDao;

    @Autowired
    private LaporanBkdFileDao laporanBkdFileDao;

    @Autowired
    @Value("${upload.penugasan}")
    private String uploadPenugasan;

    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;



    @GetMapping("/bidang/pendidikan")
    public String bidangPendidikan(Model model,
                                   @RequestParam(required = false) Semester selectSemester,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        DosenDetail dosenDetail = dosenDetailDao.findByDosen(dosen);

        if (dosenDetail == null){
            model.addAttribute("validDosen",dosen);
        }else {
            DosenDetail validDosen = dosenDetailDao.validasiKosong(dosen);
            if (validDosen != null){
                model.addAttribute("validDosen",dosen);

            }

        }


//        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());
        if(selectSemester != null){
            semester = selectSemester;
        }

        Integer ada = semesterDao.cariJadwalPelaporanAktif(semester.getId());
        List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndBidangAndSemesterOrderByTanggalInsert(StatusRecord.AKTIF, dosen, Bidang.PENDIDIKAN, semester);

        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("semester", semester);
        model.addAttribute("dosen", dosen);
        model.addAttribute("ada", ada);
        model.addAttribute("listLaporanBkd", laporanBkds);
        model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkds));

        model.addAttribute("menubidang","active");
        model.addAttribute("menubidangpendidikan","active");
        return "bidang/pendidikan/list";
    }

    @GetMapping("/bidang/pendidikan/tambah")
    public String bidangPendidikanTambah(Model model){

        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidangOrderByNamaKegiatan(StatusRecord.AKTIF, Bidang.PENDIDIKAN));
        model.addAttribute("laporanBkd",new LaporanBkd());
        model.addAttribute("menubidang","active");
        model.addAttribute("menubidangpendidikan","active");
        return "bidang/pendidikan/form";
    }

    @GetMapping("/bidang/pendidikan/edit")
    public String bidangPendidikanEdit(@RequestParam(required = true)LaporanBkd laporanBkd,
                                        Model model){

        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidangOrderByNamaKegiatan(StatusRecord.AKTIF, Bidang.PENDIDIKAN));
        model.addAttribute("laporanBkd", laporanBkd);
        model.addAttribute("menubidang","active");
        model.addAttribute("menubidangpendidikan","active");
        return "bidang/pendidikan/edit";

    }

    @PostMapping("/bidang/pendidikan/simpan")
    public String bidangPendidikanSimpan(@ModelAttribute @Valid LaporanBkd laporanBkd,
                                         @RequestParam("filePenugasan") MultipartFile filePenugasan,
                                         @RequestParam("fileKinerja") MultipartFile fileKinerja,
                                         Authentication authentication,
                                         RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());

        laporanBkd.setBidang(Bidang.PENDIDIKAN);
        laporanBkd.setDosen(dosen);
        laporanBkd.setLaporan(StatusRecord.AKTIF);
        laporanBkd.setPerbaikan(StatusRecord.NONAKTIF);
        laporanBkd.setStatus(StatusRecord.AKTIF);
        laporanBkd.setJenisKegiatan(laporanBkd.getKegiatan().getNamaKegiatan());
        laporanBkd.setTanggalInsert(LocalDateTime.now());
        laporanBkd.setSemester(semester);

        if(filePenugasan != null) {
            LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
            laporanBkd.getLaporanBkdFiles().add(laporanBkdFile);
            laporanBkdFile.setLaporanBkd(laporanBkd);
            laporanBkdFile.setStatus(StatusRecord.AKTIF);
            String namaAsli = filePenugasan.getOriginalFilename();
            Long ukuran = filePenugasan.getSize();
            String extension = "";
            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            laporanBkdFile.setFile(idFile + "." + extension);
            new File(uploadPenugasan).mkdirs();
            File tujuan = new File(uploadPenugasan + File.separator + idFile + "." + extension);
            filePenugasan.transferTo(tujuan);
            laporanBkdFile.setNamaFile(namaAsli);
            laporanBkdFile.setExtensi(extension);
            laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.PENUGASAN);
            laporanBkdFileDao.save(laporanBkdFile);
        }
        if(fileKinerja != null) {
            LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
            laporanBkd.getLaporanBkdFiles().add(laporanBkdFile);
            laporanBkdFile.setLaporanBkd(laporanBkd);
            laporanBkdFile.setStatus(StatusRecord.AKTIF);
            String namaAsli1 = fileKinerja.getOriginalFilename();
            Long ukuran1 = fileKinerja.getSize();
            String extension1 = "";
            int i = namaAsli1.lastIndexOf('.');
            int p = Math.max(namaAsli1.lastIndexOf('/'), namaAsli1.lastIndexOf('\\'));
            if (i > p) {
                extension1 = namaAsli1.substring(i + 1);
            }
            String idFile1 = UUID.randomUUID().toString();
            laporanBkdFile.setFile(idFile1 + "." + extension1);
            new File(uploadKinerja).mkdirs();
            File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension1);
            fileKinerja.transferTo(tujuan2);
            laporanBkdFile.setNamaFile(namaAsli1);
            laporanBkdFile.setExtensi(extension1);
            laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.KINERJA);
            laporanBkdFileDao.save(laporanBkdFile);
        }


        laporanBkdDao.save(laporanBkd);

        redirectAttributes.addFlashAttribute("success","Data berhasil disimpan");
        return "redirect:../pendidikan";
    }


    @PostMapping("/bidang/pendidikan/update")
    public String bidangPendidikanUpdate(@ModelAttribute @Valid LaporanBkd laporanBkd,
                                         Authentication authentication,
                                         RedirectAttributes redirectAttributes) {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());

        List<LaporanBkdFile> laporanBkdFiles = laporanBkdFileDao.findByStatusAndLaporanBkdOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF,laporanBkd);


        laporanBkd.setBidang(Bidang.PENDIDIKAN);
        laporanBkd.setDosen(dosen);
        laporanBkd.setLaporan(StatusRecord.AKTIF);
        laporanBkd.setPerbaikan(StatusRecord.AKTIF);
        laporanBkd.setStatus(StatusRecord.AKTIF);
        laporanBkd.setJenisKegiatan(laporanBkd.getKegiatan().getNamaKegiatan());
        laporanBkd.setTanggalUpdate(LocalDateTime.now());
        laporanBkd.setSemester(semester);



        laporanBkdDao.save(laporanBkd);

        for(LaporanBkdFile a : laporanBkdFiles){

            a.setLaporanBkd(laporanBkd);
            laporanBkdFileDao.save(a);
        }

        redirectAttributes.addFlashAttribute("success","Data berhasil di ubah");
        return "redirect:../pendidikan";
    }


    @PostMapping("/bidang/pendidikan/hapus")
    public String bidangPendidikanHapus(@ModelAttribute @Valid LaporanBkd laporanBkd,
                                         Authentication authentication,
                                         RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());

        laporanBkd.setStatus(StatusRecord.HAPUS);
        laporanBkd.setTanggalUpdate(LocalDateTime.now());
        laporanBkdDao.save(laporanBkd);

        redirectAttributes.addFlashAttribute("success","Data berhasil dihapus");
        return "redirect:../pendidikan";
    }


    @PostMapping("/bidang/pendidikan/tambahfilepenugasan")
    public String bidangPendidikanTambahFilePenugasan(@RequestParam LaporanBkd laporanBkd,
                                                      @RequestParam("filePenugasan") MultipartFile file,
                                                      Authentication authentication,
                                                      RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);

        LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
        laporanBkdFile.setLaporanBkd(laporanBkd);
        laporanBkdFile.setNamaFile(file.getOriginalFilename());
        String namaAsli = file.getOriginalFilename();
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        laporanBkdFile.setFile(idFile + "." + extension);
        new File(uploadPenugasan).mkdirs();
        File tujuan = new File(uploadPenugasan + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);


        laporanBkdFile.setExtensi(extension);
        laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.PENUGASAN);
        laporanBkdFile.setStatus(StatusRecord.AKTIF);
        laporanBkdFileDao.save(laporanBkdFile);

        redirectAttributes.addFlashAttribute("success", "Dokumen penugasan berhasil di tambahkan");
        return"redirect:../pendidikan";
    }


    @PostMapping("/bidang/pendidikan/tambahfilekinerja")
    public String bidangPendidikanTambahFileKinerja(@RequestParam LaporanBkd laporanBkd,
                                                      @RequestParam("fileKinerja") MultipartFile file,
                                                      Authentication authentication,
                                                      RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);

        LaporanBkdFile laporanBkdFile = new LaporanBkdFile();
        laporanBkdFile.setLaporanBkd(laporanBkd);
        laporanBkdFile.setNamaFile(file.getOriginalFilename());
        String namaAsli = file.getOriginalFilename();
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile1 = UUID.randomUUID().toString();
        laporanBkdFile.setFile(idFile1 + "." + extension);
        new File(uploadKinerja).mkdirs();
        File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);
        file.transferTo(tujuan2);

        laporanBkdFile.setExtensi(extension);
        laporanBkdFile.setJenisFileLaporan(JenisFileLaporan.KINERJA);
        laporanBkdFile.setStatus(StatusRecord.AKTIF);
        laporanBkdFileDao.save(laporanBkdFile);

        redirectAttributes.addFlashAttribute("success", "Dokumen kinerja berhasil di tambahkan");
        return"redirect:../pendidikan";
    }

    @GetMapping("/bidang/pendidikan/hapus_file")
    public String lappranBkdPendidikanHapusFile(@RequestParam(required = true)LaporanBkdFile laporanBkdFile,
                                                RedirectAttributes redirectAttributes){

        laporanBkdFile.setStatus(StatusRecord.HAPUS);
        laporanBkdFileDao.save(laporanBkdFile);


        redirectAttributes.addFlashAttribute("success", "File berhasil dihapus");
        return"redirect:../pendidikan";
    }


    @GetMapping("/laporanbkd/{laporanBkdFile}/filekinerja/")
    public ResponseEntity<byte[]> tampilkanLaporanBkdFileKinerja(@PathVariable LaporanBkdFile laporanBkdFile) throws Exception {
        String lokasiFile = uploadKinerja + File.separator + laporanBkdFile.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (laporanBkdFile.getFile().toLowerCase().endsWith("jpeg") || laporanBkdFile.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (laporanBkdFile.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (laporanBkdFile.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/laporanbkd/{laporanBkdFile}/filepenugasan/")
    public ResponseEntity<byte[]> tampilkanLaporanBkdFilePenugasan(@PathVariable LaporanBkdFile laporanBkdFile) throws Exception {
        String lokasiFile = uploadPenugasan + File.separator + laporanBkdFile.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (laporanBkdFile.getFile().toLowerCase().endsWith("jpeg") || laporanBkdFile.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (laporanBkdFile.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (laporanBkdFile.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}

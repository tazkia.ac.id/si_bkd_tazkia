package id.tazkia.sibkd.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.config.RoleDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.config.Role;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class KopertaisPenggunaController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/kopertais/pengguna")
    public String kopertaisPengguna(Model model,
                         @PageableDefault(size = 20)Pageable pageable,
                         @RequestParam(required = false)Role roleSearch,
                         @RequestParam(required = false)String search) {

        if(roleSearch == null){
            if(search == null) {
                model.addAttribute("listUser", userDao.findByRoleJenisAndUsernameNot("kopertais", "suprayogi@tazkia.ac.id" , pageable));
            }else{
                model.addAttribute("ada", search);
                model.addAttribute("listUser", userDao.findByRoleJenisAndUsernameNotAndUsernameContainingIgnoreCaseOrRoleJenisAndUsernameNotAndNamaLengkapContainingIgnoreCaseOrderByNamaLengkap("kopertais", "suprayogi@tazkia.ac.id", search, "kopertais","suprayogi@tazkia.ac.id", search, pageable));
            }
        }else{
            if(search == null) {
                model.addAttribute("roleSelected", roleSearch);
                model.addAttribute("listUser", userDao.findByRoleAndUsernameNotOrderByNamaLengkap(roleSearch, "suprayogi@tazkia.ac.id", pageable));
            }else{
                model.addAttribute("roleSelected", roleSearch);
                model.addAttribute("ada", search);
                model.addAttribute("listUser", userDao.findByRoleAndUsernameNotAndUsernameContainingIgnoreCaseOrRoleAndUsernameNotAndNamaLengkapContainingIgnoreCaseOrderByNamaLengkap(roleSearch,"suprayogi@tazkia.ac.id", search, roleSearch, "suprayogi@tazkia.ac.id", search, pageable));
            }
        }

        model.addAttribute("listRole", roleDao.findByJenis("kopertais"));
        model.addAttribute("menukopertaispengguna", "active");
        return "kopertais/pengguna/list";
    }

    @GetMapping("/kopertais/pengguna/baru")
    public String kopertaisPenggunaBaru(Model model){

        model.addAttribute("user", new User());
        model.addAttribute("listRole", roleDao.findByJenis("kopertais"));
        model.addAttribute("menukopertaispengguna", "active");
        return "kopertais/pengguna/form";
    }

    @GetMapping("/kopertais/pengguna/edit")
    public String kopertaisPenggunaEdit(Model model,
                                        @RequestParam(required = true)User user){

        model.addAttribute("user", user);
        model.addAttribute("listRole", roleDao.findByJenis("kopertais"));
        model.addAttribute("menukopertaispengguna", "active");
        return "kopertais/pengguna/form";
    }

    @PostMapping("/kopertais/pengguna/simpan")
    public String kopertaisPenggunaSipan(@ModelAttribute @Valid User user,
                                         Authentication authentication,
                                         RedirectAttributes redirectAttributes){

        User currentUser = currentUserService.currentUser(authentication);

//        user.setActive(true);
        user.setUserUpdate(currentUser.getNamaLengkap());
        user.setDateUpdate(LocalDateTime.now());
        userDao.save(user);

        redirectAttributes.addFlashAttribute("success","Data berhasil disimpan..");
        return "redirect:../pengguna";
    }



}

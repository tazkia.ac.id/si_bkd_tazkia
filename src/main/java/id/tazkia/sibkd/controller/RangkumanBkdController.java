package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.DosenDao;
import id.tazkia.sibkd.dao.DosenDetailDao;
import id.tazkia.sibkd.dao.LaporanBkdDao;
import id.tazkia.sibkd.dao.SemesterDao;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.Semester;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

@Controller
public class RangkumanBkdController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private SemesterDao semesterDao;

    @Autowired
    private DosenDetailDao dosenDetailDao;
    @Autowired
    private LaporanBkdDao laporanBkdDao;

    @GetMapping("/rangkuman/bkd")
    public String rangkumanBkd(Model model,
                               @RequestParam(required = false) Semester selectSemester,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);

//        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());

        if(selectSemester != null){
            semester = selectSemester;
        }

        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("semester", semester);
        model.addAttribute("dosen", dosen);
        model.addAttribute("dosenDetail", dosenDetailDao.findByStatusAndDosen(StatusRecord.AKTIF, dosen));
        model.addAttribute("rangkumanBkd", laporanBkdDao.rangkumanBkd(dosen.getId(), semester.getId()));

        model.addAttribute("menuRangkumanBkd","active");
        return "rangkuman_bkd/list";
    }

}

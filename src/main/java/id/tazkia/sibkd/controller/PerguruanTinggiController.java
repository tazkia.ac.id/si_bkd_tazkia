package id.tazkia.sibkd.controller;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.PerguruanTinggiDao;
import id.tazkia.sibkd.entity.PerguruanTinggi;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class PerguruanTinggiController {

    @Autowired
    private PerguruanTinggiDao perguruanTinggiDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/perguruan_tinggi")
    public String perguruanTinggi(Model model,
                                  @PageableDefault(size = 20) Pageable pageable,
                                  @RequestParam(required = false)String cari){

        if (cari != null) {
            model.addAttribute("ada", cari);
            model.addAttribute("listPerguruanTinggi", perguruanTinggiDao.findByNamaPerguruanTinggiContainingIgnoreCaseAndStatusNotInOrAlamatContainingIgnoreCaseAndStatusNotIn(cari,Arrays.asList(StatusRecord.HAPUS),cari,Arrays.asList(StatusRecord.HAPUS), pageable));
        }else{
            model.addAttribute("listPerguruanTinggi", perguruanTinggiDao.findByStatusNotInOrderByNamaPerguruanTinggi(Arrays.asList(StatusRecord.HAPUS),pageable));
        }
        model.addAttribute("menusetting", "active");
        model.addAttribute("menuperguruantinggi","active");
        return "perguruan_tinggi/list";
    }

    @GetMapping("/perguruan_tinggi/baru")
    public String tambahPerguruanTinggi(Model model){

        model.addAttribute("perguruanTinggi", new PerguruanTinggi());

        model.addAttribute("menusetting", "active");
        model.addAttribute("menuperguruantinggi","active");
        return "perguruan_tinggi/form";
    }

    @GetMapping("/perguruan_tinggi/edit")
    public String editPerguruanTinggi(Model model,
                                      @RequestParam(required = true)PerguruanTinggi perguruanTinggi){

        model.addAttribute("perguruanTinggi", perguruanTinggi);

        model.addAttribute("menusetting", "active");
        model.addAttribute("menuperguruantinggi","active");
        return "perguruan_tinggi/form";
    }

    @PostMapping("/perguruan_tinggi/simpan")
    public String simpanPerguruanTinggi(@ModelAttribute @Valid PerguruanTinggi perguruanTinggi,
                                        RedirectAttributes redirectAttributes,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);

//        perguruanTinggi.setStatus(StatusRecord.AKTIF);
        perguruanTinggi.setTanggalUpdate(LocalDateTime.now());
        perguruanTinggi.setUserUpdate(user.getNamaLengkap());
        perguruanTinggiDao.save(perguruanTinggi);

        redirectAttributes.addFlashAttribute("success","Data perguruan tinggi berhasil disimpan");
        return "redirect:../perguruan_tinggi";
    }



}

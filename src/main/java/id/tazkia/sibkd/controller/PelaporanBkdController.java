package id.tazkia.sibkd.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lowagie.text.DocumentException;

import id.tazkia.sibkd.dao.DosenDao;
import id.tazkia.sibkd.dao.DosenDetailDao;
import id.tazkia.sibkd.dao.LaporanBkdDao;
import id.tazkia.sibkd.dao.LaporanBkdFileDao;
import id.tazkia.sibkd.dao.SemesterDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.dto.RangkumanBkdDto;
import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.DosenDetail;
import id.tazkia.sibkd.entity.LaporanBkd;
import id.tazkia.sibkd.entity.LaporanBkdFile;
import id.tazkia.sibkd.entity.Semester;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.export.LaporanBkdPdf;
import id.tazkia.sibkd.services.CurrentUserService;
import id.tazkia.sibkd.services.GmailApiService;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class PelaporanBkdController {

    @Autowired
    private SemesterDao semesterDao;

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private LaporanBkdDao laporanBkdDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private DosenDetailDao dosenDetailDao;

    @Autowired
    private LaporanBkdFileDao laporanBkdFileDao;

    @Autowired
    private GmailApiService gmailApiService;

    @Autowired
    @Value("${upload.penugasan}")
    private String uploadPenugasan;

    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;
    @Autowired
    private UserDao userDao;


    @GetMapping("/pelaporan_bkd")
    public String pelaporanBkd(Model model,
                               @RequestParam(required = false) String selectStatus,
                               @RequestParam(required = false) String search,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);

//        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());
//        if(selectSemester != null){
//            semester = selectSemester;
//        }

        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        if (search == null || search.isEmpty()){
            if(selectStatus == null || selectStatus.isEmpty()) {
                model.addAttribute("pelaporanBkd", laporanBkdDao.pelaporanBkd(semester.getId()));
            }else{
                model.addAttribute("selectStatus", selectStatus);
                model.addAttribute("pelaporanBkd", laporanBkdDao.pelaporanBkdDenganStatus(semester.getId(), selectStatus));
            }
        }else {
            if(selectStatus == null || selectStatus.isEmpty()) {
                model.addAttribute("search", search);
                model.addAttribute("pelaporanBkd", laporanBkdDao.pelaporanBkdCari(semester.getId(), search));
            }else{
                model.addAttribute("search", search);
                model.addAttribute("selectStatus", selectStatus);
                model.addAttribute("pelaporanBkd", laporanBkdDao.pelaporanBkdCariDenganStatus(semester.getId(), search, selectStatus));
            }
        }

        model.addAttribute("semester", semester);
        model.addAttribute("menupelaporanbkd","active");
        return "pelaporan_bkd/list";
    }

    @GetMapping("/pelaporan_bkd/pelaporanDetail")
    public String pelaporanBkdDetail(Model model, @RequestParam(required = true) Dosen dosen,
                                     @RequestParam(required = false) Semester selectSemester,
                                     @RequestParam(required = false) Bidang bidang,
                                     Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());
        if(selectSemester != null){
            semester = selectSemester;
        }
        System.out.println("Test : " + dosen.getId());
//        String asesor = "1";
//        if(dosen.getAsesorSatu() != null && dosen.getAsesorSatu().getId().equals(user.getId())){
//            asesor = "1";
//        }else if(dosen.getAsesorDua() != null && dosen.getAsesorDua().getId().equals(user.getId())){
//            asesor = "2";
//        }

        if(bidang != null){
            model.addAttribute("bidangSelected", bidang);
            List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndSemesterAndBidangOrderByTanggalInsert(StatusRecord.AKTIF, dosen, semester, bidang);
            model.addAttribute("listLaporanBkd", laporanBkds);
            model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkds));
        }else {
            List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndSemesterOrderByTanggalInsert(StatusRecord.AKTIF, dosen, semester);
            model.addAttribute("listLaporanBkd", laporanBkds);
            model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkds));

        }
        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("semester", semester);
        model.addAttribute("bidang", Bidang.values());
        model.addAttribute("dosen", dosen);
//        model.addAttribute("asesor", asesor);
        model.addAttribute("dosenDetail", dosenDetailDao.findByDosen(dosen));

        return "pelaporan_bkd/detail";
    }



    @GetMapping("/pelaporan_bkd/download")
    public void generatePdfFile(HttpServletResponse response,
                                @RequestParam(required = true) Dosen dosen,
                                @RequestParam(required = false) Semester semester) throws DocumentException, IOException {

        DosenDetail dosenDetail = dosenDetailDao.findByStatusAndDosen(StatusRecord.AKTIF, dosen);

        response.setContentType("application/pdf");

        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");

        String currentDateTime = dateFormat.format(new Date());

        String headerkey = "Content-Disposition";

        String headervalue = "attachment; filename=Laporan_BKD_" + dosen.getNama() +"_" + currentDateTime + ".pdf";

        response.setHeader(headerkey, headervalue);

        List<RangkumanBkdDto> rangkumanBkdDtos = laporanBkdDao.rangkumanBkd(dosen.getId(),semester.getId());

        List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndSemesterOrderByTanggalInsert(StatusRecord.AKTIF, dosen, semester);

        List<LaporanBkdFile> laporanBkdFiles = laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsertAscJenisFileLaporanDesc(StatusRecord.AKTIF,laporanBkds);

        LaporanBkdPdf generator = new LaporanBkdPdf();

        generator.generate(rangkumanBkdDtos, dosen, semester, dosenDetail, laporanBkds, laporanBkdFiles, uploadPenugasan, uploadKinerja, response);

    }

    @GetMapping("/pelaporanBkd/remind")
    public String remindPost(@RequestParam Dosen dosen, @RequestParam String status, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
//        if (status.equals("BA")){
//
//        }

        if (status.equals("BI")){
            gmailApiService.kirimEmail(
                    user.getUsername(),
                    dosen.getEmail(),
                    "Info Pengisian BKD : ",
                    "Pengisian Laporan BKD sudah di buka silahkan segera akses https://bkd.kopertais2.id"
            );
        }

        if (status.equals("BS")){

            gmailApiService.kirimEmail(
                    user.getUsername(),
                    dosen.getAsesorSatu().getUsername(),
                    "Info Penilaian BKD : ",
                    "Penilaian Laporan BKD sudah di buka silahkan segera akses https://bkd.kopertais2.id"
            );

            gmailApiService.kirimEmail(
                    "arifrahmatullah@tazkia.ac.id",
                    dosen.getAsesorDua().getUsername(),
                    "Test - Info Penilaian BKD : " + LocalDateTime.now(),
                    "Penilaian Laporan BKD sudah di buka silahkan segera akses https://bkd.kopertais2.id"
            );
        }

        return "redirect:/pelaporan_bkd";
    }


}

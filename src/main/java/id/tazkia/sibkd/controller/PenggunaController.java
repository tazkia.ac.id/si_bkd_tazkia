package id.tazkia.sibkd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.config.RoleDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.config.User;
import jakarta.validation.Valid;

@Controller
public class PenggunaController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/pengguna")
    public String listPengguna(Model model,
                               @PageableDefault(size = 20)Pageable pageable){

        model.addAttribute("listUser", userDao.findByRoleJenis("kopertais", pageable));
        model.addAttribute("menusetting", "active");
        model.addAttribute("menupengguna","active");
        return "pengguna/list";

    }

    @GetMapping("/pengguna/baru")
    public String penggunaBaru(Model model){

        model.addAttribute("user", new User());
        model.addAttribute("listRole", roleDao.findByJenis("kopertais"));

        model.addAttribute("menusetting", "active");
        model.addAttribute("menupengguna","active");
        return "pengguna/form";
    }

    @GetMapping("/pengguna/edit")
    public String penggunaEdit(Model model,
                               @RequestParam(required = true) User user){

        model.addAttribute("user", user);
        model.addAttribute("listRole", roleDao.findByJenis("kopertais"));

        model.addAttribute("menusetting", "active");
        model.addAttribute("menupengguna","active");
        return "pengguna/form";
    }

    @PostMapping("/pengguna/simpan")
    public String simpanPengguna(@ModelAttribute @Valid User user,
                                 RedirectAttributes redirectAttributes){

        user.setActive(true);
        userDao.save(user);

        redirectAttributes.addFlashAttribute("success", "Data Berhasil disimpan");
        return "redirect:../pengguna";
    }
}

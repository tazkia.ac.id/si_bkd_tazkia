package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.SemesterDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;

@Controller
public class HomeController {

//    private static final Logger LOGGER = LoggerFactory.getLogger();

    @Autowired
    private SemesterDao semesterDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    @Value("${upload.pedoman}")
    private String pedomanBkd;

    @GetMapping("/beranda")
    public String home(Model model,
                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);


        model.addAttribute("sekarang", LocalDateTime.now());
        model.addAttribute("userAktif", user);
        model.addAttribute("pedoman",pedomanBkd+"/pedoman.pdf");
        model.addAttribute("semesterAktif", semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now()));
        model.addAttribute("semesterTerakhir", semesterDao.findFirstByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("menuhome", "active");
        return "home";
    }

//    @GetMapping("/login")
//    public String login(){
//
//
//        return "login";
//    }
//

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("loginError", true);
        return "login2";
    }

    @GetMapping("/")
    public String formAwal(){
        return "redirect:/beranda";
    }

    @GetMapping("/404")
    public String form404(){

        return "error";
    }


    @GetMapping("/pedoman/bkd/")
    public ResponseEntity<byte[]> tampilkanBuktiPembayaran() throws Exception {

        String pedoman = "pedoman.pdf";
        String lokasiFile = pedomanBkd + File.separator + pedoman;
//        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pedoman.toLowerCase().endsWith("jpeg") || pedoman.toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pedoman.toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pedoman.toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
//            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}

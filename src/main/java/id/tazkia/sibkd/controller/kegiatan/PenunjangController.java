package id.tazkia.sibkd.controller.kegiatan;

import id.tazkia.sibkd.dao.KegiatanDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Kegiatan;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class PenunjangController {
    @Autowired
    private KegiatanDao kegiatanDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/kegiatan/penunjang")
    public String kegiatanPenunjang(Model model, @PageableDefault(size = 20)Pageable pageable){
        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidang(StatusRecord.AKTIF, Bidang.PENUNJANG, pageable));

        model.addAttribute("bidang","penunjang");
        model.addAttribute("keterangan","Daftar Kegiatan Bidang Penunjang");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpenunjang", "active");
        return "kegiatan/list";
    }

    @GetMapping("/kegiatan/penunjang/baru")
    public String kegiatanPenunjangBaru(Model model){

        model.addAttribute("kegiatan", new Kegiatan());
        model.addAttribute("bidang","penunjang");
        model.addAttribute("keterangan","Kegiatan Bidang Penunjang");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpenunjang", "active");
        return "kegiatan/form";

    }

    @GetMapping("/kegiatan/penunjang/edit")
    public String kegiatanPenelitianEdit(Model model,
                                         @RequestParam(required = true)Kegiatan kegiatan){

        model.addAttribute("kegiatan", kegiatan);
        model.addAttribute("bidang","penunjang");
        model.addAttribute("keterangan","Kegiatan Bidang Penunjang");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpenunjang", "active");
        return "kegiatan/form";

    }

    @PostMapping("/kegiatan/penunjang/simpan")
    public String kegiatanPeneliatianSimpan(@ModelAttribute @Valid Kegiatan kegiatan,
                                            Authentication authentication,
                                            RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        kegiatan.setBidang(Bidang.PENUNJANG);
        kegiatan.setStatus(StatusRecord.AKTIF);
        kegiatanDao.save(kegiatan);

        redirectAttributes.addFlashAttribute("success","Simpan data kegiatan bidang penunjang.");
        return "redirect:../penunjang";
    }

    @PostMapping("/kegiatan/penunjang/hapus")
    public String kegiatanPenelitianHapus(@ModelAttribute @Valid Kegiatan kegiatan,
                                          Authentication authentication,
                                          RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        kegiatan.setStatus(StatusRecord.HAPUS);
        kegiatanDao.save(kegiatan);

        redirectAttributes.addFlashAttribute("hapus","Hapus data kegiatan bidang penunjang.");
        return "redirect:../penunjang";
    }
}

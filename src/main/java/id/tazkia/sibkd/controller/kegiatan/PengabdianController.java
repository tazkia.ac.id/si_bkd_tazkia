package id.tazkia.sibkd.controller.kegiatan;

import id.tazkia.sibkd.dao.KegiatanDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Kegiatan;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class PengabdianController {

    @Autowired
    private KegiatanDao kegiatanDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/kegiatan/pengabdian")
    public String kegiatanPengabdian(Model model, @PageableDefault(size = 20)Pageable pageable){
        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidang(StatusRecord.AKTIF, Bidang.PENGABDIAN, pageable));

        model.addAttribute("bidang","pengabdian");
        model.addAttribute("keterangan","Daftar Kegiatan Bidang Pengabdian");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpengabdian", "active");
        return "kegiatan/list";
    }

    @GetMapping("/kegiatan/pengabdian/baru")
    public String kegiatanPengabdianBaru(Model model){

        model.addAttribute("kegiatan", new Kegiatan());
        model.addAttribute("bidang","pengabdian");
        model.addAttribute("keterangan","Kegiatan Bidang Pengabdian");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpengabdian", "active");
        return "kegiatan/form";

    }

    @GetMapping("/kegiatan/pengabdian/edit")
    public String kegiatanPenelitianEdit(Model model,
                                         @RequestParam(required = true)Kegiatan kegiatan){

        model.addAttribute("kegiatan", kegiatan);
        model.addAttribute("bidang","pengabdian");
        model.addAttribute("keterangan","Kegiatan Bidang Peneliatian");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpengabdian", "active");
        return "kegiatan/form";

    }

    @PostMapping("/kegiatan/pengabdian/simpan")
    public String kegiatanPeneliatianSimpan(@ModelAttribute @Valid Kegiatan kegiatan,
                                            Authentication authentication,
                                            RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        kegiatan.setBidang(Bidang.PENGABDIAN);
        kegiatan.setStatus(StatusRecord.AKTIF);
        kegiatanDao.save(kegiatan);

        redirectAttributes.addFlashAttribute("success","Simpan data kegiatan bidang pengabdian.");
        return "redirect:../pengabdian";
    }

    @PostMapping("/kegiatan/pengabdian/hapus")
    public String kegiatanPenelitianHapus(@ModelAttribute @Valid Kegiatan kegiatan,
                                          Authentication authentication,
                                          RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        kegiatan.setStatus(StatusRecord.HAPUS);
        kegiatanDao.save(kegiatan);

        redirectAttributes.addFlashAttribute("hapus","Hapus data kegiatan bidang pengabdian.");
        return "redirect:../pengabdian";
    }


}

package id.tazkia.sibkd.controller.kegiatan;

import id.tazkia.sibkd.dao.KegiatanDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Kegiatan;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class PendidikanController {

    @Autowired
    private KegiatanDao kegiatanDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/kegiatan/pendidikan")
    public String kegiatanPendidikan(Model model,
                                     @PageableDefault(size = 20)Pageable pageable){

        model.addAttribute("listKegiatan", kegiatanDao.findByStatusAndBidang(StatusRecord.AKTIF, Bidang.PENDIDIKAN, pageable));

        model.addAttribute("bidang","pendidikan");
        model.addAttribute("keterangan","Daftar Kegiatan Bidang Pendidikan dan Pengajaran");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpendidikan", "active");
        return "kegiatan/list";
    }

    @GetMapping("/kegiatan/pendidikan/baru")
    public String kegiatanPendidikanBaru(Model model){

        model.addAttribute("kegiatan", new Kegiatan());
        model.addAttribute("bidang","pendidikan");
        model.addAttribute("keterangan","Kegiatan Bidang Pendidikan dan Pengajaran");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpendidikan", "active");
        return "kegiatan/form";

    }

    @GetMapping("/kegiatan/pendidikan/edit")
    public String kegiatanPendidikanEdit(Model model,
                                         @RequestParam(required = true)Kegiatan kegiatan){

        model.addAttribute("kegiatan", kegiatan);
        model.addAttribute("bidang","pendidikan");
        model.addAttribute("keterangan","Kegiatan Bidang Pendidikan dan Pengajaran");
        model.addAttribute("menukegiatan","active");
        model.addAttribute("menukegiatanpendidikan", "active");
        return "kegiatan/form";

    }

    @PostMapping("/kegiatan/pendidikan/simpan")
    public String kegiatanPendidikanSimpan(@ModelAttribute @Valid Kegiatan kegiatan,
                                           Authentication authentication,
                                           RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        kegiatan.setBidang(Bidang.PENDIDIKAN);
        kegiatan.setStatus(StatusRecord.AKTIF);
        kegiatanDao.save(kegiatan);

        redirectAttributes.addFlashAttribute("success","Simpan data kegiatan bidang pendidikan dan pengajaran berhasil.");
        return "redirect:../pendidikan";
    }

    @PostMapping("/kegiatan/pendidikan/hapus")
    public String kegiatanPendidikanHapus(@ModelAttribute @Valid Kegiatan kegiatan,
                                           Authentication authentication,
                                           RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        kegiatan.setStatus(StatusRecord.HAPUS);
        kegiatanDao.save(kegiatan);

        redirectAttributes.addFlashAttribute("hapus","Hapus data kegiatan bidang pendidikan dan pengajaran berhasil.");
        return "redirect:../pendidikan";
    }


    @GetMapping("/api/kegiatan/detail")
    @ResponseBody
    public Kegiatan cariDetailKegiatan(@RequestParam(required = false) String id){

        Kegiatan kegiatan= kegiatanDao.findById(id).get();

        return kegiatan;
    }

}

package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.SemesterDao;
import id.tazkia.sibkd.entity.Semester;
import id.tazkia.sibkd.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;

@Controller
public class SemesterController {

    @Autowired
    private SemesterDao semesterDao;

    @GetMapping("/semester")
    public String listSemester(Model model){

        model.addAttribute("semester", new Semester());

        model.addAttribute("menusetting", "active");
        model.addAttribute("menusemester", "active");
        model.addAttribute("list", semesterDao.findByStatusOrderByTanggalInsertDesc(StatusRecord.AKTIF));
        return "semester/list";
    }

    @GetMapping("/semester/tambah")
    public String tambahSemester(Model model){

        model.addAttribute("semester",new Semester());
        model.addAttribute("menusetting", "active");
        model.addAttribute("menusemester", "active");
        return "semester/form";
    }

    @PostMapping("/semester/simpan")
    public String postTambahSemester(@Valid Semester semester, RedirectAttributes attributes){

        semester.setStatus(StatusRecord.AKTIF);
        semester.setTanggalInsert(LocalDate.now());
        semesterDao.save(semester);
        attributes.addFlashAttribute("berhasil", "berhasil");

        return "redirect:/semester";
    }

    @GetMapping("/semester/ubah")
    public String ubahSemester(Model model,
                               @RequestParam(required = true)Semester semester){

        model.addAttribute("semester", semester);

        model.addAttribute("menusetting", "active");
        model.addAttribute("menusemester", "active");
        return "semester/form";
    }

}

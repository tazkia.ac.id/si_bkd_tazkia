package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.AdminPtDao;
import id.tazkia.sibkd.dao.AssesmentAllDao;
import id.tazkia.sibkd.dao.AssesmentAllDetailDao;
import id.tazkia.sibkd.dao.SemesterDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.dao.config.UserPasswordDao;
import id.tazkia.sibkd.entity.AssesmentAll;
import id.tazkia.sibkd.entity.Rekomendasi;
import id.tazkia.sibkd.entity.Semester;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;

@Controller
public class AssesmentAllController {

    @Autowired
    private SemesterDao semesterDao;

    @Autowired
    private AssesmentAllDao assesmentAllDao;

    @Autowired
    private AssesmentAllDetailDao assesmentAllDetailDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private AdminPtDao adminPtDao;
    @Autowired
    private UserPasswordDao userPasswordDao;


    @GetMapping("/assesment/all")
    public String assesmentAll(Model model,
                               @PageableDefault(size = 20) Pageable page,
                               @RequestParam(required = false) Semester selectSemester){

        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now());

        if(selectSemester != null){
            semester = selectSemester;
        }

        model.addAttribute("listAssesmentAll", assesmentAllDao.findByStatusOrderByTanggalDesc(StatusRecord.AKTIF,page));
        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("semester", semester);
        model.addAttribute("listRekomendasi", Rekomendasi.values());
        model.addAttribute("assesmentAll", new AssesmentAll());

        return "assesment_all/form";
    }

    @PostMapping("/assesment/all/simpan")
    public String assesmentAllSimpan(@ModelAttribute AssesmentAll assesmentAll,
                                     Authentication authentication,
                                     RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);

        assesmentAll.setTanggal(LocalDateTime.now());
        assesmentAll.setStatus(StatusRecord.AKTIF);
        assesmentAll.setUser(user.getUsername());
        assesmentAll.setStatusAssesment(StatusRecord.WAITING);
        assesmentAllDao.save(assesmentAll);

        return "redirect:../all";
    }

}

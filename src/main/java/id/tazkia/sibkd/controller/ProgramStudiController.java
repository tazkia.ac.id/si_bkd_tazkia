package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.ProgramStudiDao;
import id.tazkia.sibkd.entity.ProgramStudi;
import id.tazkia.sibkd.entity.StatusRecord;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class ProgramStudiController {
    private final ProgramStudiDao programStudiDao;

    public ProgramStudiController(ProgramStudiDao programStudiDao) {
        this.programStudiDao = programStudiDao;
    }

    @GetMapping("/prodi")
    public String programStudi(Model model,
                               @RequestParam(required = false)String search,
                               @PageableDefault(size = 10)Pageable pageable){

        if(StringUtils.hasText(search)){
            model.addAttribute("search", search);
            model.addAttribute("listProgramStudi", programStudiDao.findByStatusAndNamaProgramStudiContainingIgnoreCaseOrderByNamaProgramStudi(StatusRecord.AKTIF,search, pageable));
        }else{
            model.addAttribute("listProgramStudi", programStudiDao.findByStatusOrderByNamaProgramStudi(StatusRecord.AKTIF, pageable));
        }

        model.addAttribute("programStudi", new ProgramStudi());
        model.addAttribute("menuprodi", "active");
        return "prodi/list";
    }

    @PostMapping("/prodi/simpan")
    public String programStudiSimpan(@ModelAttribute @Valid ProgramStudi programStudi,
                                     Authentication authentication,
                                     RedirectAttributes redirectAttributes){

        programStudi.setStatus(StatusRecord.AKTIF);
        programStudiDao.save(programStudi);

        redirectAttributes.addFlashAttribute("success", "Data Berhasil disimpan");
        return "redirect:../prodi";
    }

    @PostMapping("/prodi/update")
    public String programStudiUpdate(@RequestParam(required = false) String namaProgramStudi,
                                     @RequestParam(required = true) String id,
                                     Authentication authentication,
                                     RedirectAttributes redirectAttributes){

        ProgramStudi programStudi = programStudiDao.findByStatusAndId(StatusRecord.AKTIF, id);
        programStudi.setNamaProgramStudi(namaProgramStudi);
        programStudi.setStatus(StatusRecord.AKTIF);
        programStudiDao.save(programStudi);

        redirectAttributes.addFlashAttribute("success", "Data Berhasil diubah");
        return "redirect:../prodi";
    }

}

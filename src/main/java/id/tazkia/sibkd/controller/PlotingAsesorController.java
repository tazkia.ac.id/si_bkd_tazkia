package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.DosenDao;
import id.tazkia.sibkd.dao.PerguruanTinggiDao;
import id.tazkia.sibkd.dao.config.RoleDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.PerguruanTinggi;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PlotingAsesorController {

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private PerguruanTinggiDao perguruanTinggiDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/ploting/asesor")
    public String plotingAsesor(Model model,
                                @RequestParam(required = false)String cari,
                                @RequestParam(required = false)PerguruanTinggi perguruanTinggi,
                                @PageableDefault(size = 20)Pageable pageable){


        if(perguruanTinggi == null) {
            if(cari == null){
                model.addAttribute("listDosen", dosenDao.findByStatusOrderByNama(StatusRecord.AKTIF, pageable));
            }else{
                model.addAttribute("ada", cari);
                model.addAttribute("listDosen", dosenDao.findByStatusAndNamaContainingIgnoreCaseOrderByNama(StatusRecord.AKTIF, cari, pageable));
            }
        }else{
            model.addAttribute("perguruanTinggiSelected", perguruanTinggi);
            if(cari == null) {
                model.addAttribute("listDosen", dosenDao.findByStatusAndPerguruanTinggiOrderByNama(StatusRecord.AKTIF, perguruanTinggi, pageable));
            }else{
                model.addAttribute("ada", cari);
                model.addAttribute("listDosen", dosenDao.findByStatusAndPerguruanTinggiAndNamaContainingIgnoreCaseOrderByNama(StatusRecord.AKTIF, perguruanTinggi, cari, pageable));
            }
        }

        model.addAttribute("listPerguruanTinggi", perguruanTinggiDao.findByStatusOrderByNamaPerguruanTinggi(StatusRecord.AKTIF));
        model.addAttribute("listAsesor", userDao.findByRoleOrderByNamaLengkap(roleDao.findById("asesor").get()));
        model.addAttribute("menuplotingasesor","active");
        return "ploting/asesor";
    }


    @PostMapping("/ploting/asesor/save")
    public String plotingAsesorSave(@RequestParam(required = true) String id,
                                    @RequestParam(required = false) String asesor1,
                                    @RequestParam(required = false) String asesor2){

        Dosen dosen = dosenDao.findById(id).get();

        if(asesor1 == null || asesor1.equals("")){
            dosen.setAsesorSatu(null);
        }else{
            dosen.setAsesorSatu(userDao.findById(asesor1).get());
        }

        if(asesor2 == null || asesor2.equals("")){
            dosen.setAsesorDua(null);
        }else {
            dosen.setAsesorDua(userDao.findById(asesor2).get());
        }
        dosenDao.save(dosen);

        return "redirect:../asesor";
    }

}

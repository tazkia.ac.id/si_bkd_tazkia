package id.tazkia.sibkd.controller;

import id.tazkia.sibkd.dao.*;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.*;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class AssesmentController {

    @Autowired
    private UserDao userDao;
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private LaporanBkdDao laporanBkdDao;

    @Autowired
    private SemesterDao semesterDao;

    @Autowired
    private LaporanBkdFileDao laporanBkdFileDao;

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private DosenDetailDao dosenDetailDao;

    @GetMapping("/assesment")
    public String assesment(Model model,
                            @RequestParam(required = false) String status,
                            @RequestParam(required = false) String search,
                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());

        Integer ada = semesterDao.cariJadwalAssesmentAktif(semester.getId());
        System.out.println("status :"+ status);
        System.out.println("search :"+ search);
        if(StringUtils.hasText(status)){
            if(StringUtils.hasText(search)){
                System.out.println("jalan dengan parameter status dan search");
                model.addAttribute("search", search);
                model.addAttribute("listAssesment", laporanBkdDao.listAssesmentStatusSearch(user.getId(),semester.getId(), status, search));
            }else{
                System.out.println("jalan dengan parameter status");
                model.addAttribute("listAssesment", laporanBkdDao.listAssesmentStatus(user.getId(),semester.getId(), status));
            }
        }else{
            if(StringUtils.hasText(search)){
                System.out.println("jalan dengan parameter search");
                model.addAttribute("search", search);
                model.addAttribute("listAssesment", laporanBkdDao.listAssesmentSearch(user.getId(), semester.getId(), search));
            }else {
                System.out.println("Jalan tanpa parameter");
                model.addAttribute("listAssesment", laporanBkdDao.listAssesment(user.getId(), semester.getId()));
            }
        }

        if(StringUtils.hasText(status)){

            if(status.equals("BI")){
                model.addAttribute("selectBi","active");
            }else if(status.equals("BN")){
                model.addAttribute("selectBn","active");
            }else if (status.equals("BSN")){
                model.addAttribute("selectBsn","active");
            }else if (status.equals("SE")){
                model.addAttribute("selectSe","active");
            }else{
                model.addAttribute("selectSemua","active");
            }

        }else{
            model.addAttribute("selectSemua","active");
        }

        model.addAttribute("ada", ada);
        model.addAttribute("menuassesment","active");
        return "assesment/list";
    }

    @GetMapping("/assesment/detail")
    public String assesmentDetail(Model model,
                                  @RequestParam(required = false) Semester selectSemester,
                                  @RequestParam(required = false) Bidang bidang,
                                  @RequestParam(required = true) Dosen dosen,
                                  Authentication authentication){
        User user = currentUserService.currentUser(authentication);

        //        Semester semester = semesterDao.findByStatusAndTanggalBukaSemesterIsBeforeAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF, LocalDateTime.now(),LocalDateTime.now());
        Semester semester = semesterDao.findTopByStatusAndTanggalTutupSemesterIsAfter(StatusRecord.AKTIF,LocalDateTime.now());
        if(selectSemester != null){
            semester = selectSemester;
        }

        String asesor = "1";
        if(dosen.getAsesorSatu().getId().equals(user.getId())){
            asesor = "1";
        }else if(dosen.getAsesorDua().getId().equals(user.getId())){
            asesor = "2";
        }

        if(bidang != null){
            model.addAttribute("bidangSelected", bidang);
            List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndSemesterAndBidangOrderByTanggalInsert(StatusRecord.AKTIF, dosen, semester, bidang);
            model.addAttribute("listLaporanBkd", laporanBkds);
            model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkds));
        }else {
            List<LaporanBkd> laporanBkds = laporanBkdDao.findByStatusAndDosenAndSemesterOrderByTanggalInsert(StatusRecord.AKTIF, dosen, semester);
            model.addAttribute("listLaporanBkd", laporanBkds);
            model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdInOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkds));

        }
        model.addAttribute("listSemester", semesterDao.findByStatusOrderByTanggalBukaSemesterDesc(StatusRecord.AKTIF));
        model.addAttribute("semester", semester);
        model.addAttribute("bidang", Bidang.values());
        model.addAttribute("dosen", dosen);
        model.addAttribute("asesor", asesor);
        model.addAttribute("dosenDetail", dosenDetailDao.findByDosen(dosen));

        model.addAttribute("menuassesment","active");
        return "assesment/detail";

    }

    @GetMapping("/assesment/detail/edit")
    public String assesmentDetail(Model model,
                                  @RequestParam(required = true) LaporanBkd laporanBkd,
                                  Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        String asesor = "1";
        if(laporanBkd.getDosen().getAsesorSatu().getId().equals(user.getId())){
            asesor = "1";
        }else if(laporanBkd.getDosen().getAsesorDua().getId().equals(user.getId())){
            asesor = "2";
        }
        model.addAttribute("listRekomendasi", Rekomendasi.values());
        model.addAttribute("asesor", asesor);
        model.addAttribute("laporanBkd", laporanBkd);
        model.addAttribute("listLaporanBkdFile", laporanBkdFileDao.findByStatusAndLaporanBkdOrderByLaporanBkdTanggalInsert(StatusRecord.AKTIF, laporanBkd));
        model.addAttribute("menuassesment","active");
        return "assesment/edit";
    }

    @PostMapping("/assesment/detail/simpan")
    public String assesmentDetailSave(@RequestParam LaporanBkd laporanBkd,
                                      @RequestParam(required = false) Rekomendasi rekomendasi,
                                      @RequestParam(required = false) String catatanAsesor,
                                      @RequestParam(required = false) BigDecimal sksAsesor,
                                      Authentication authentication,
                                      RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        String asesor = "1";
        if(laporanBkd.getDosen().getAsesorSatu().getId().equals(user.getId())){
            asesor = "1";
        }else if(laporanBkd.getDosen().getAsesorDua().getId().equals(user.getId())){
            asesor = "2";
        }
        LaporanBkd laporanBkd1 = laporanBkdDao.findByStatusAndId(StatusRecord.AKTIF, laporanBkd.getId());
        if(asesor.equals("1")){
            laporanBkd1.setCatatanAsesor1(catatanAsesor);
            laporanBkd1.setRekomendasiAsesor1(rekomendasi);
            laporanBkd1.setSksTerpenuhiAsesor1(sksAsesor);
            laporanBkd1.setAsesor1(user);
        }else{
            laporanBkd1.setCatatanAsesor2(catatanAsesor);
            laporanBkd1.setRekomendasiAsesor2(rekomendasi);
            laporanBkd1.setSksTerpenuhiAsesor2(sksAsesor);
            laporanBkd1.setAsesor2(user);
        }
        laporanBkdDao.save(laporanBkd1);

        redirectAttributes.addFlashAttribute("success","Penilaian berhasil disimpan");
        return "redirect:../detail?dosen="+laporanBkd1.getDosen().getId();

    }

}

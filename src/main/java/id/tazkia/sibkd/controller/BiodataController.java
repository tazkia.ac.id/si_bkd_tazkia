package id.tazkia.sibkd.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.tazkia.sibkd.dao.DosenDao;
import id.tazkia.sibkd.dao.DosenDetailDao;
import id.tazkia.sibkd.dao.JabatanStrukturalDao;
import id.tazkia.sibkd.dao.PerguruanTinggiDao;
import id.tazkia.sibkd.dao.ProgramStudiDao;
import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.dto.BiodataDto;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.DosenDetail;
import id.tazkia.sibkd.entity.StatusRecord;
import id.tazkia.sibkd.entity.config.User;
import id.tazkia.sibkd.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class BiodataController {
    @Autowired
    private UserDao userDao;
    @Autowired
    private DosenDao dosenDao;
    @Autowired
    private DosenDetailDao dosenDetailDao;
    @Autowired
    private PerguruanTinggiDao perguruanTinggiDao;
    @Autowired
    private JabatanStrukturalDao jabatanStrukturalDao;
    @Autowired
    private ProgramStudiDao programStudiDao;
    @Autowired
    private CurrentUserService currentUserService;
    @Value("${upload.biodata}")
    private String biodataFolder;


    @GetMapping("/biodata")
    public String biodata(Model model,Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        DosenDetail dosenDetail = dosenDetailDao.findByDosen(dosen);

        if (dosenDetail != null){
            BiodataDto biodataDto = new BiodataDto();
            biodataDto.setId(dosenDetail.getId());
            biodataDto.setIdDosen(dosen.getId());
            biodataDto.setNama(dosen.getNama());
            biodataDto.setProfil(dosenDetail.getFoto());
            biodataDto.setEmail(dosen.getEmail());
            biodataDto.setTelepon(dosen.getTelepon());
            biodataDto.setNamaProdi(dosenDetail.getProdi().getNamaProgramStudi());
            biodataDto.setJabatanStruktural(dosen.getJabatanStruktural());
            biodataDto.setPerguruanTinggi(dosen.getPerguruanTinggi());
            BeanUtils.copyProperties(dosenDetail,biodataDto);
            model.addAttribute("biodata", biodataDto);

        }else {
            BiodataDto biodataDto = new BiodataDto();
            biodataDto.setIdDosen(dosen.getId());
            biodataDto.setNama(dosen.getNama());
            biodataDto.setEmail(dosen.getEmail());
            biodataDto.setTelepon(dosen.getTelepon());
            biodataDto.setPerguruanTinggi(dosen.getPerguruanTinggi());
            biodataDto.setJabatanStruktural(dosen.getJabatanStruktural());
            model.addAttribute("biodata", biodataDto);
        }


        model.addAttribute("dosen", dosen);
         model.addAttribute("listAsesor", userDao.findByRoleNameOrderByNamaLengkap("ASESOR"));
        model.addAttribute("menubiodata", "active");
        return "biodata/view";
    }
    @GetMapping("/biodata/edit")
    public String biodataEdit(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByUser(user);
        DosenDetail dosenDetail = dosenDetailDao.findByDosen(dosen);

        if (dosenDetail != null){
            BiodataDto biodataDto = new BiodataDto();
            biodataDto.setId(dosenDetail.getId());
            biodataDto.setIdDosen(dosen.getId());
            biodataDto.setNama(dosen.getNama());
            biodataDto.setProfil(dosenDetail.getFoto());
            biodataDto.setEmail(dosen.getEmail());
            biodataDto.setNamaProdi(dosenDetail.getProdi().getNamaProgramStudi());
            biodataDto.setTelepon(dosen.getTelepon());
            biodataDto.setJabatanStruktural(dosen.getJabatanStruktural());
            biodataDto.setPerguruanTinggi(dosen.getPerguruanTinggi());
            BeanUtils.copyProperties(dosenDetail,biodataDto);
            model.addAttribute("biodata", biodataDto);

        }else {
            BiodataDto biodataDto = new BiodataDto();
            biodataDto.setIdDosen(dosen.getId());
            biodataDto.setNama(dosen.getNama());
            biodataDto.setEmail(dosen.getEmail());
            biodataDto.setTelepon(dosen.getTelepon());
            biodataDto.setPerguruanTinggi(dosen.getPerguruanTinggi());
            model.addAttribute("biodata", biodataDto);
        }


        model.addAttribute("dosen", dosen);
        // model.addAttribute("asesorSatu", dosen.getAsesorSatu());
        // model.addAttribute("asesorDua", dosen.getAsesorDua());
        model.addAttribute("perguruan", perguruanTinggiDao.findByStatusOrderByNamaPerguruanTinggi(StatusRecord.AKTIF));
        model.addAttribute("menubiodata", "active");
        model.addAttribute("prodi", programStudiDao.findByStatusOrderByNamaProgramStudi(StatusRecord.AKTIF));
        model.addAttribute("listJabatan", jabatanStrukturalDao.findByStatus(StatusRecord.AKTIF));
       

        return "biodata/form";
    }

    @PostMapping("/biodata/simpan")
    public String simpanBiodata(@Valid BiodataDto biodataDto, MultipartFile kartuNidn, MultipartFile sertifikatDosen, MultipartFile foto,
                                MultipartFile skDt, MultipartFile skJafung, MultipartFile ijazahs1, MultipartFile ijazahs2, MultipartFile ijazahs3)throws  Exception{

        DosenDetail detailDaoByDosen = dosenDetailDao.findByDosen(dosenDao.findById(biodataDto.getIdDosen()).get());
        if (detailDaoByDosen == null) {

            DosenDetail dosenDetail = new DosenDetail();
            dosenDetail.setDosen(dosenDao.findById(biodataDto.getIdDosen()).get());
            dosenDetail.getDosen().setNama(biodataDto.getNama());
            dosenDetail.getDosen().setJabatanStruktural(biodataDto.getJabatanStruktural());
            dosenDetail.getDosen().setTelepon(biodataDto.getTelepon());
            dosenDetail.setNidn(biodataDto.getNidn());
            dosenDetail.setNomorSertifikat(biodataDto.getNomorSertifikat());
            dosenDetail.setFakultas(biodataDto.getFakultas());
            dosenDetail.setProdi(biodataDto.getProdi());
            dosenDetail.setAlamat(biodataDto.getAlamat());
            dosenDetail.setJabatanFungsional(biodataDto.getJabatanFungsional());
            dosenDetail.setJudulSkripsi(biodataDto.getJudulSkripsi());
            dosenDetail.setJudulTesis(biodataDto.getJudulTesis());
            dosenDetail.setJudulDisertasi(biodataDto.getJudulDisertasi());
            dosenDetail.setPerguruans1(biodataDto.getJudulDisertasi());
            dosenDetail.setPerguruans2(biodataDto.getPerguruans2());
            dosenDetail.setPerguruans3(biodataDto.getPerguruans3());


            if (kartuNidn.getSize() > 0) {
                String namaAsli = kartuNidn.getOriginalFilename();


//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                kartuNidn.transferTo(tujuan);


                dosenDetail.setKartuNidn(idFile + "." + extension);
                dosenDetail.setNamaKartuNidn(namaAsli);

            } else {
                dosenDetail.setKartuNidn(dosenDetail.getKartuNidn());
                dosenDetail.setNamaKartuNidn(dosenDetail.getNamaKartuNidn());
            }

            if (sertifikatDosen.getSize() > 0) {
                String namaAsli = sertifikatDosen.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                sertifikatDosen.transferTo(tujuan);


                dosenDetail.setSertifikatDosen(idFile + "." + extension);
                dosenDetail.setNamaSertifikatDosen(namaAsli);

            } else {
                dosenDetail.setSertifikatDosen(dosenDetail.getSertifikatDosen());
                dosenDetail.setNamaSertifikatDosen(dosenDetail.getNamaSertifikatDosen());
            }

            if (foto.getSize() > 0) {
                String namaAsli = foto.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                foto.transferTo(tujuan);


                dosenDetail.setFoto(idFile + "." + extension);
                dosenDetail.setNamaFoto(namaAsli);

            } else {
                dosenDetail.setFoto(dosenDetail.getFoto());
                dosenDetail.setNamaFoto(dosenDetail.getNamaFoto());
            }

            if (skDt.getSize() > 0) {
                String namaAsli = skDt.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                skDt.transferTo(tujuan);


                dosenDetail.setSkDt(idFile + "." + extension);
                dosenDetail.setNamaSkdt(namaAsli);

            } else {
                dosenDetail.setSkDt(dosenDetail.getSkDt());
                dosenDetail.setNamaSkdt(dosenDetail.getNamaSkdt());
            }

            if (skJafung.getSize() > 0) {
                String namaAsli = skJafung.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                skJafung.transferTo(tujuan);


                dosenDetail.setSkJafung(idFile + "." + extension);
                dosenDetail.setNamaJafung(namaAsli);

            } else {
                dosenDetail.setSkJafung(dosenDetail.getSkJafung());
                dosenDetail.setNamaJafung(dosenDetail.getNamaJafung());
            }

            if (ijazahs1.getSize() > 0) {
                String namaAsli = ijazahs1.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                ijazahs1.transferTo(tujuan);


                dosenDetail.setIjazahs1(idFile + "." + extension);
                dosenDetail.setNamaIjazahs1(namaAsli);

            } else {
                dosenDetail.setIjazahs1(dosenDetail.getIjazahs1());
                dosenDetail.setNamaIjazahs1(dosenDetail.getNamaIjazahs1());
            }

            if (ijazahs2.getSize() > 0) {
                String namaAsli = ijazahs2.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                ijazahs2.transferTo(tujuan);


                dosenDetail.setIjazahs2(idFile + "." + extension);
                dosenDetail.setNamaIjazahs2(namaAsli);

            } else {
                dosenDetail.setIjazahs2(dosenDetail.getIjazahs2());
                dosenDetail.setNamaIjazahs2(dosenDetail.getNamaIjazahs2());
            }

            if (ijazahs3.getSize() > 0) {
                String namaAsli = ijazahs3.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                ijazahs3.transferTo(tujuan);


                dosenDetail.setIjazahs3(idFile + "." + extension);
                dosenDetail.setIjazahs3(namaAsli);

            } else {
                dosenDetail.setIjazahs3(dosenDetail.getIjazahs3());
                dosenDetail.setNamaIjazahs3(dosenDetail.getNamaIjazahs3());
            }


            dosenDetailDao.save(dosenDetail);
        }else {
            DosenDetail dosenDetail = dosenDetailDao.findById(biodataDto.getId()).get();
            dosenDetail.setDosen(dosenDao.findById(biodataDto.getIdDosen()).get());
            dosenDetail.getDosen().setNama(biodataDto.getNama());
            dosenDetail.getDosen().setJabatanStruktural(biodataDto.getJabatanStruktural());
            dosenDetail.getDosen().setTelepon(biodataDto.getTelepon());
            dosenDetail.setNidn(biodataDto.getNidn());
            dosenDetail.setNomorSertifikat(biodataDto.getNomorSertifikat());
            dosenDetail.setFakultas(biodataDto.getFakultas());
            dosenDetail.setProdi(biodataDto.getProdi());
            dosenDetail.setAlamat(biodataDto.getAlamat());
            dosenDetail.setJabatanFungsional(biodataDto.getJabatanFungsional());
            dosenDetail.setJudulSkripsi(biodataDto.getJudulSkripsi());
            dosenDetail.setJudulTesis(biodataDto.getJudulTesis());
            dosenDetail.setJudulDisertasi(biodataDto.getJudulDisertasi());
            dosenDetail.setPerguruans1(biodataDto.getPerguruans1());
            dosenDetail.setPerguruans2(biodataDto.getPerguruans2());
            dosenDetail.setPerguruans3(biodataDto.getPerguruans3());

            if (foto.getSize() > 0) {
                String namaAsli = foto.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                foto.transferTo(tujuan);


                dosenDetail.setFoto(idFile + "." + extension);
                dosenDetail.setNamaFoto(namaAsli);

            } else {
                dosenDetail.setFoto(dosenDetail.getFoto());
                dosenDetail.setNamaFoto(dosenDetail.getNamaFoto());
            }


            if (kartuNidn.getSize() > 0) {
                String namaAsli = kartuNidn.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                kartuNidn.transferTo(tujuan);


                dosenDetail.setKartuNidn(idFile + "." + extension);
                dosenDetail.setNamaKartuNidn(namaAsli);

            } else {
                dosenDetail.setKartuNidn(dosenDetail.getKartuNidn());
                dosenDetail.setNamaKartuNidn(dosenDetail.getNamaKartuNidn());
            }

            if (sertifikatDosen.getSize() > 0) {
                String namaAsli = sertifikatDosen.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                sertifikatDosen.transferTo(tujuan);


                dosenDetail.setSertifikatDosen(idFile + "." + extension);
                dosenDetail.setNamaSertifikatDosen(namaAsli);

            } else {
                dosenDetail.setSertifikatDosen(dosenDetail.getSertifikatDosen());
                dosenDetail.setNamaSertifikatDosen(dosenDetail.getNamaSertifikatDosen());
            }

            if (skDt.getSize() > 0) {
                String namaAsli = skDt.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                skDt.transferTo(tujuan);


                dosenDetail.setSkDt(idFile + "." + extension);
                dosenDetail.setNamaSkdt(namaAsli);

            } else {
                dosenDetail.setSkDt(dosenDetail.getSkDt());
                dosenDetail.setNamaSkdt(dosenDetail.getNamaSkdt());
            }

            if (skJafung.getSize() > 0) {
                String namaAsli = skJafung.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                skJafung.transferTo(tujuan);


                dosenDetail.setSkJafung(idFile + "." + extension);
                dosenDetail.setNamaJafung(namaAsli);

            } else {
                dosenDetail.setSkJafung(dosenDetail.getSkJafung());
                dosenDetail.setNamaJafung(dosenDetail.getNamaJafung());
            }

            if (ijazahs1.getSize() > 0) {
                String namaAsli = ijazahs1.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                ijazahs1.transferTo(tujuan);


                dosenDetail.setIjazahs1(idFile + "." + extension);
                dosenDetail.setNamaIjazahs1(namaAsli);

            } else {
                dosenDetail.setIjazahs1(dosenDetail.getIjazahs1());
                dosenDetail.setNamaIjazahs1(dosenDetail.getNamaIjazahs1());
            }

            if (ijazahs2.getSize() > 0) {
                String namaAsli = ijazahs2.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                ijazahs2.transferTo(tujuan);


                dosenDetail.setIjazahs2(idFile + "." + extension);
                dosenDetail.setNamaIjazahs2(namaAsli);

            } else {
                dosenDetail.setIjazahs2(dosenDetail.getIjazahs2());
                dosenDetail.setNamaIjazahs2(dosenDetail.getNamaIjazahs2());
            }

            if (ijazahs3.getSize() > 0) {
                String namaAsli = ijazahs3.getOriginalFilename();

//        memisahkan extensi
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }


                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = biodataFolder + File.separator + biodataDto.getNama();
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                ijazahs3.transferTo(tujuan);


                dosenDetail.setIjazahs3(idFile + "." + extension);
                dosenDetail.setNamaIjazahs3(namaAsli);


            } else {
                dosenDetail.setIjazahs3(dosenDetail.getIjazahs3());
                dosenDetail.setNamaIjazahs3(dosenDetail.getNamaIjazahs3());
            }


            dosenDetailDao.save(dosenDetail);
        }

       return "redirect:../biodata";
    }

     @PostMapping("/biodata/ganti-asesor")
     public String gantiAsesor(@ModelAttribute @Valid Dosen dosen, RedirectAttributes redirectAttributes){

        Dosen dosen2 = dosenDao.findById(dosen.getId()).get();
        dosen2.setAsesorSatu(dosen.getAsesorSatu());
        dosen2.setAsesorDua(dosen.getAsesorDua());
        dosenDao.save(dosen2);

        redirectAttributes.addFlashAttribute("success", "Pergantian asesor berhasil");
        return "redirect:../biodata";
     }

    @GetMapping("/upload/{dosenDetail}/biodata/")
    public ResponseEntity<byte[]> foto(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getFoto();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getFoto().toLowerCase().endsWith("jpeg") || dosenDetail.getFoto().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getFoto().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

    @GetMapping("/upload/{dosenDetail}/kartu/")
    public ResponseEntity<byte[]> kartu(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getKartuNidn();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getKartuNidn().toLowerCase().endsWith("jpeg") || dosenDetail.getKartuNidn().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getKartuNidn().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getKartuNidn().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

    @GetMapping("/upload/{dosenDetail}/sertifikat/")
    public ResponseEntity<byte[]> sertifikat(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getSertifikatDosen();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getSertifikatDosen().toLowerCase().endsWith("jpeg") || dosenDetail.getSertifikatDosen().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getSertifikatDosen().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getSertifikatDosen().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }


    @GetMapping("/upload/{dosenDetail}/skdt/")
    public ResponseEntity<byte[]> skDt(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getSkDt();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getSkDt().toLowerCase().endsWith("jpeg") || dosenDetail.getSkDt().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getSkDt().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getSkDt().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

    @GetMapping("/upload/{dosenDetail}/jafung/")
    public ResponseEntity<byte[]> jafung(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getSkJafung();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getSkJafung().toLowerCase().endsWith("jpeg") || dosenDetail.getSkJafung().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getSkJafung().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getSkJafung().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

    @GetMapping("/upload/{dosenDetail}/s1/")
    public ResponseEntity<byte[]> s1(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getIjazahs1();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getIjazahs1().toLowerCase().endsWith("jpeg") || dosenDetail.getIjazahs1().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getIjazahs1().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getIjazahs1().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

    @GetMapping("/upload/{dosenDetail}/s2/")
    public ResponseEntity<byte[]> s2(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getIjazahs2();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getIjazahs2().toLowerCase().endsWith("jpeg") || dosenDetail.getIjazahs2().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getIjazahs2().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getIjazahs2().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

    @GetMapping("/upload/{dosenDetail}/s3/")
    public ResponseEntity<byte[]> s3(@PathVariable DosenDetail dosenDetail) throws Exception {
        String lokasiFile = biodataFolder + File.separator + dosenDetail.getDosen().getNama()
                + File.separator + dosenDetail.getIjazahs3();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosenDetail.getIjazahs3().toLowerCase().endsWith("jpeg") || dosenDetail.getIjazahs3().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosenDetail.getIjazahs3().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (dosenDetail.getIjazahs3().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();


        }

    }

}

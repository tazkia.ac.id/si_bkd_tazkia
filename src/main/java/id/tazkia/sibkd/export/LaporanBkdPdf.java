package id.tazkia.sibkd.export;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import id.tazkia.sibkd.dto.RangkumanBkdDto;
import id.tazkia.sibkd.entity.Bidang;
import id.tazkia.sibkd.entity.Dosen;
import id.tazkia.sibkd.entity.DosenDetail;
import id.tazkia.sibkd.entity.JenisFileLaporan;
import id.tazkia.sibkd.entity.LaporanBkd;
import id.tazkia.sibkd.entity.LaporanBkdFile;
import id.tazkia.sibkd.entity.Semester;
import jakarta.servlet.http.HttpServletResponse;

public class LaporanBkdPdf {


    private List<RangkumanBkdDto> rangkumanBkdDtos;


    public void generate(List<RangkumanBkdDto> rangkumanBkdDtos, Dosen dosen, Semester semester, DosenDetail dosenDetail, List<LaporanBkd> laporanBkds, List<LaporanBkdFile> laporanBkdFiles, String uploadPenugasan, String uploadKinerja, HttpServletResponse response) throws DocumentException, IOException {

        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();

        Image background = Image.getInstance("src/main/resources/static/img/logo_kemenag2.png");
        background.setAbsolutePosition(0, 0);
        background.scaleAbsolute(document.getPageSize().getWidth(), document.getPageSize().getHeight());
        document.add(background);

        Font fontTiltle = FontFactory.getFont(FontFactory.HELVETICA);
        Font fontTiltle2 = FontFactory.getFont(FontFactory.HELVETICA);
        Font fontTiltle3 = FontFactory.getFont(FontFactory.HELVETICA);
        Font fontTiltle4 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

        fontTiltle.setSize(20);
        fontTiltle2.setSize(14);
        fontTiltle3.setSize(10);
        fontTiltle4.setSize(10);

        // Creating paragraph
        Paragraph paragraph1 = new Paragraph("Laporan BKD Dosen", fontTiltle);
        Paragraph paragraph2 = new Paragraph("Rangkuman BKD", fontTiltle2);
        Paragraph paragraph3 = new Paragraph("Nama Dosen   : " + dosen.getNama(), fontTiltle3);
        Paragraph paragraph4 = new Paragraph("NIDN               : " + dosenDetail.getNidn(), fontTiltle3);
        Paragraph paragraph5 = new Paragraph("Status Dosen  : " + dosen.getJabatanStruktural().getNamaJabatanStruktural(), fontTiltle3);
        Paragraph paragraph6 = new Paragraph("Semester/TA   : " + semester.getNamaSemester(), fontTiltle3);
        Paragraph paragraph7 = new Paragraph("Asesor 1          : " + dosen.getAsesorSatu().getNamaLengkap(), fontTiltle3);
        Paragraph paragraph8 = new Paragraph("Asesor 2          : " + dosen.getAsesorDua().getNamaLengkap(), fontTiltle3);

        Paragraph paragraphKosong = new Paragraph("");
        // Aligning the paragraph in the document

        paragraph1.setAlignment(Paragraph.ALIGN_CENTER);
        paragraph1.setSpacingAfter(10);
        paragraph2.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph2.setSpacingAfter(10);
        paragraph3.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph4.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph5.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph6.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph7.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph8.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph8.setSpacingAfter(10);

        // Adding the created paragraph in the document

        document.add(paragraph1);
        document.bottomMargin();
        document.add(paragraph2);
        document.add(paragraphKosong);
        document.add(paragraph3);
        document.add(paragraph4);
        document.add(paragraph5);
        document.add(paragraph6);
        document.add(paragraph7);
        document.add(paragraph8);
        document.add(paragraphKosong);
        PdfPTable table = new PdfPTable(8);
        table.setWidthPercentage(100);
        table.setWidths(new int[] {2,3,3,3,3,3,3,3});
        table.setSpacingBefore(5);
        PdfPCell cell;
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(CMYKColor.WHITE);
        font.setSize(10);

        Font font2 = FontFactory.getFont(FontFactory.HELVETICA);
        font2.setColor(CMYKColor.DARK_GRAY);
        font2.setSize(8);

        cell = new PdfPCell(new Phrase("No.",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Kegitan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Syarat",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Rangkuman BKD",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(4);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Dosen", font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Asesor 1", font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Asesor 2", font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Status", font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        // Iterating the list of students

        for (RangkumanBkdDto rangkumanBkdDto1 : rangkumanBkdDtos) {

            // Adding student id
            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getNomor()), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setRowspan(2);
            table.addCell(cell);
            // Adding student name

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getKegiatan()), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setRowspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getKetentuan()), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setRowspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getKinerja()) + " SKS", font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setRowspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getDosenTercapai()), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getSksTerpenuhiAsesor1()) + " SKS", font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getSksTerpenuhiAsesor2()) + " SKS", font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            String statusAkhir = "M";
            if(rangkumanBkdDto1.getStatusAkhir().equals("M")){
                statusAkhir = "Memenuhi";
            }else{
                statusAkhir = "Tidak Memenuhi";
            }

            cell = new PdfPCell(new Phrase(String.valueOf(statusAkhir), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setRowspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getAsesorTercapai1()), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(rangkumanBkdDto1.getAsesorTercapai2()), font2));
            cell.setPadding(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);


        }

        // Adding the created table to the document
        document.add(table);

        document.add(paragraphKosong);
        Paragraph paragraph11 = new Paragraph("Keterangan", fontTiltle4);
        Paragraph paragraph14 = new Paragraph("M : Memenuhi", fontTiltle3);
        Paragraph paragraph15 = new Paragraph("T : Tidak Memenuhi", fontTiltle3);paragraph2.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph11.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph14.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph15.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph11);
        document.add(paragraph14);
        document.add(paragraph15);

        //Pendidikan ===================================================================================================

        document.newPage();

        document.add(paragraph1);
        Paragraph paragraph16 = new Paragraph(semester.getNamaSemester(),fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_CENTER);
        paragraph16.setSpacingAfter(10);
        document.add(paragraph16);
        paragraph16 = new Paragraph("Bidang Pendidikan", fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_LEFT);
        paragraph16.setSpacingAfter(10);
        document.add(paragraph16);

        PdfPTable tablePendidikan = new PdfPTable(7);
        tablePendidikan.setWidthPercentage(100);
        tablePendidikan.setWidths(new int[] {2,3,3,3,3,3,3});
        tablePendidikan.setSpacingBefore(5);

        cell = new PdfPCell(new Phrase("No.",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePendidikan.addCell(cell);
        cell = new PdfPCell(new Phrase("Jenis Kegiatan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePendidikan.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePendidikan.addCell(cell);

        cell = new PdfPCell(new Phrase("Masa Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePendidikan.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePendidikan.addCell(cell);


        cell = new PdfPCell(new Phrase("Bukti Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePendidikan.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePendidikan.addCell(cell);

        cell = new PdfPCell(new Phrase("Bukti Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePendidikan.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePendidikan.addCell(cell);


        Integer a = 0;
        for (LaporanBkd pendidikan : laporanBkds) {

            if (pendidikan.getBidang().equals(Bidang.PENDIDIKAN)){
                String catatan = "-";
                String rekomendasi = "-";
                String sksAsesor1 = "-";
            a = a + 1;
            String nomor = a.toString();
            // Adding student id
            cell = new PdfPCell(new Phrase(String.valueOf(a), font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setRowspan(4);
            tablePendidikan.addCell(cell);
            // Adding student name

            cell = new PdfPCell(new Phrase(String.valueOf(pendidikan.getNamaKegiatan()), font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablePendidikan.addCell(cell);


            String filePenugasan = pendidikan.getBuktiPenugasan();
            for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                if(laporanBkdFile.getLaporanBkd() == pendidikan && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)){
                    filePenugasan = filePenugasan + "\n - " + laporanBkdFile.getNamaFile();
                }
            }

            cell = new PdfPCell(new Phrase(filePenugasan, font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablePendidikan.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(pendidikan.getJumlahSks()) + " SKS", font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablePendidikan.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(pendidikan.getMasaPenugasan()) + " Semester", font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablePendidikan.addCell(cell);

            String fileKinerja = pendidikan.getBuktiKinerja();
            for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                if(laporanBkdFile.getLaporanBkd() == pendidikan && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)){
                    fileKinerja = fileKinerja + "\n - " + laporanBkdFile.getNamaFile();
                }
            }
            cell = new PdfPCell(new Phrase(fileKinerja, font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablePendidikan.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(pendidikan.getJumlahSks()) + " SKS", font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablePendidikan.addCell(cell);


            if(pendidikan.getCatatanAsesor1() != null) {
                catatan = "- " + pendidikan.getCatatanAsesor1();
            }else{
                catatan ="-";
            }
            cell = new PdfPCell(new Phrase("Catatan Asesor 1 : \n" + catatan, font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setColspan(3);
            tablePendidikan.addCell(cell);

            if(pendidikan.getRekomendasiAsesor1() != null) {
                rekomendasi = "- " + pendidikan.getRekomendasiAsesor1().toString();
            }else{
                rekomendasi = "-";
            }
            cell = new PdfPCell(new Phrase("Rekomendasi Asesor 1 : \n" + rekomendasi, font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setColspan(2);
            tablePendidikan.addCell(cell);

            if(pendidikan.getSksTerpenuhiAsesor1() != null) {
                sksAsesor1 = pendidikan.getSksTerpenuhiAsesor1().toString();
            }else{
                sksAsesor1 = "-";
            }
            cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablePendidikan.addCell(cell);


            if(pendidikan.getCatatanAsesor2() != null) {
                catatan = "- " + pendidikan.getCatatanAsesor2();
            }else{
                catatan = "-";
            }
            cell = new PdfPCell(new Phrase("Catatan Asesor 2 : \n" + catatan, font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setColspan(3);
            tablePendidikan.addCell(cell);

            if(pendidikan.getRekomendasiAsesor2() != null) {
                rekomendasi = "- " + pendidikan.getRekomendasiAsesor2().toString();
            }else{
                rekomendasi = "-";
            }
            cell = new PdfPCell(new Phrase("Rekomendasi Asesor 2 : \n" + rekomendasi, font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setColspan(2);
            tablePendidikan.addCell(cell);

            if(pendidikan.getSksTerpenuhiAsesor2() != null) {
                sksAsesor1 = pendidikan.getSksTerpenuhiAsesor2().toString();
            }else{
                sksAsesor1 = "-";
            }
            cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablePendidikan.addCell(cell);


            cell = new PdfPCell(new Phrase("Catatan Kaprodi : \n", font2));
            cell.setPadding(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setColspan(6);
            tablePendidikan.addCell(cell);

             }
        }

        document.add(tablePendidikan);



        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENDIDIKAN)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)) {
                    String lokasiFile = uploadPenugasan + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Penugasan : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {

                    }

                }else{

                }

            }
        }

        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENDIDIKAN)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)) {
                    String lokasiFile = uploadKinerja + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 50));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Kinerja : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }


        //Penelitian===================================================================================================================

        document.newPage();

        document.add(paragraph1);
        paragraph16 = new Paragraph(semester.getNamaSemester(),fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_CENTER);
        paragraph16.setSpacingAfter(10);
        document.add(paragraph16);
        paragraph16 = new Paragraph("Bidang Penelitian", fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph16);

        PdfPTable tablePenelitian = new PdfPTable(7);
        tablePenelitian.setWidthPercentage(100);
        tablePenelitian.setWidths(new int[] {2,3,3,3,3,3,3});
        tablePenelitian.setSpacingBefore(5);

        cell = new PdfPCell(new Phrase("No.",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePenelitian.addCell(cell);
        cell = new PdfPCell(new Phrase("Jenis Kegiatan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePenelitian.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePenelitian.addCell(cell);

        cell = new PdfPCell(new Phrase("Masa Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePenelitian.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePenelitian.addCell(cell);


        cell = new PdfPCell(new Phrase("Bukti Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenelitian.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenelitian.addCell(cell);

        cell = new PdfPCell(new Phrase("Bukti Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenelitian.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenelitian.addCell(cell);

        a = 0;
        for (LaporanBkd penelitian : laporanBkds) {

            if (penelitian.getBidang().equals(Bidang.PENELITIAN)){
                String catatan = "-";
                String rekomendasi = "-";
                String sksAsesor1 = "-";
                a = a + 1;
                String nomor = a.toString();
                // Adding student id
                cell = new PdfPCell(new Phrase(String.valueOf(a), font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setRowspan(4);
                tablePenelitian.addCell(cell);
                // Adding student name

                cell = new PdfPCell(new Phrase(String.valueOf(penelitian.getNamaKegiatan()), font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePenelitian.addCell(cell);


                String filePenugasan = penelitian.getBuktiPenugasan();
                for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                    if(laporanBkdFile.getLaporanBkd() == penelitian && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)){
                        filePenugasan = filePenugasan + "\n - " + laporanBkdFile.getNamaFile();
                    }
                }

                cell = new PdfPCell(new Phrase(filePenugasan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePenelitian.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(penelitian.getJumlahSks()) + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenelitian.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(penelitian.getMasaPenugasan()) + " Semester", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenelitian.addCell(cell);

                String fileKinerja = penelitian.getBuktiKinerja();
                for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                    if(laporanBkdFile.getLaporanBkd() == penelitian && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)){
                        fileKinerja = fileKinerja + "\n - " + laporanBkdFile.getNamaFile();
                    }
                }
                cell = new PdfPCell(new Phrase(fileKinerja, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePenelitian.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(penelitian.getJumlahSks()) + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenelitian.addCell(cell);


                if(penelitian.getCatatanAsesor1() != null) {
                    catatan = "- " + penelitian.getCatatanAsesor1();
                }else{
                    catatan ="-";
                }
                cell = new PdfPCell(new Phrase("Catatan Asesor 1 : \n" + catatan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                tablePenelitian.addCell(cell);

                if(penelitian.getRekomendasiAsesor1() != null) {
                    rekomendasi = "- " + penelitian.getRekomendasiAsesor1().toString();
                }else{
                    rekomendasi = "-";
                }
                cell = new PdfPCell(new Phrase("Rekomendasi Asesor 1 : \n" + rekomendasi, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(2);
                tablePenelitian.addCell(cell);

                if(penelitian.getSksTerpenuhiAsesor1() != null) {
                    sksAsesor1 = penelitian.getSksTerpenuhiAsesor1().toString();
                }else{
                    sksAsesor1 = "-";
                }
                cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenelitian.addCell(cell);


                if(penelitian.getCatatanAsesor2() != null) {
                    catatan = "- " + penelitian.getCatatanAsesor2();
                }else{
                    catatan = "-";
                }
                cell = new PdfPCell(new Phrase("Catatan Asesor 2 : \n" + catatan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                tablePenelitian.addCell(cell);

                if(penelitian.getRekomendasiAsesor2() != null) {
                    rekomendasi = "- " + penelitian.getRekomendasiAsesor2().toString();
                }else{
                    rekomendasi = "-";
                }
                cell = new PdfPCell(new Phrase("Rekomendasi Asesor 2 : \n" + rekomendasi, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(2);
                tablePenelitian.addCell(cell);

                if(penelitian.getSksTerpenuhiAsesor2() != null) {
                    sksAsesor1 = penelitian.getSksTerpenuhiAsesor2().toString();
                }else{
                    sksAsesor1 = "-";
                }
                cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenelitian.addCell(cell);


                cell = new PdfPCell(new Phrase("Catatan Kaprodi : \n", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(6);
                tablePenelitian.addCell(cell);

            }
        }

        document.add(tablePenelitian);

        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENELITIAN)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)) {
                    String lokasiFile = uploadPenugasan + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Penugasan : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }

        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENELITIAN)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)) {
                    String lokasiFile = uploadKinerja + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 50));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Kinerja : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }


        //Pengabdian===================================================================================================

        document.newPage();

        document.add(paragraph1);
        paragraph16 = new Paragraph(semester.getNamaSemester(),fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_CENTER);
        paragraph16.setSpacingAfter(10);
        document.add(paragraph16);
        paragraph16 = new Paragraph("Bidang Pengabdian", fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph16);

        PdfPTable tablePengabdian = new PdfPTable(7);
        tablePengabdian.setWidthPercentage(100);
        tablePengabdian.setWidths(new int[] {2,3,3,3,3,3,3});
        tablePengabdian.setSpacingBefore(5);

        cell = new PdfPCell(new Phrase("No.",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePengabdian.addCell(cell);
        cell = new PdfPCell(new Phrase("Jenis Kegiatan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePengabdian.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePengabdian.addCell(cell);

        cell = new PdfPCell(new Phrase("Masa Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePengabdian.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePengabdian.addCell(cell);


        cell = new PdfPCell(new Phrase("Bukti Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePengabdian.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePengabdian.addCell(cell);

        cell = new PdfPCell(new Phrase("Bukti Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePengabdian.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePengabdian.addCell(cell);

        a = 0;
        for (LaporanBkd pengabdian : laporanBkds) {

            if (pengabdian.getBidang().equals(Bidang.PENGABDIAN)){
                String catatan = "-";
                String rekomendasi = "-";
                String sksAsesor1 = "-";
                a = a + 1;
                String nomor = a.toString();
                // Adding student id
                cell = new PdfPCell(new Phrase(String.valueOf(a), font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setRowspan(4);
                tablePengabdian.addCell(cell);
                // Adding student name

                cell = new PdfPCell(new Phrase(String.valueOf(pengabdian.getNamaKegiatan()), font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePengabdian.addCell(cell);


                String filePenugasan = pengabdian.getBuktiPenugasan();
                for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                    if(laporanBkdFile.getLaporanBkd() == pengabdian && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)){
                        filePenugasan = filePenugasan + "\n - " + laporanBkdFile.getNamaFile();
                    }
                }

                cell = new PdfPCell(new Phrase(filePenugasan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePengabdian.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(pengabdian.getJumlahSks()) + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePengabdian.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(pengabdian.getMasaPenugasan()) + " Semester", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePengabdian.addCell(cell);

                String fileKinerja = pengabdian.getBuktiKinerja();
                for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                    if(laporanBkdFile.getLaporanBkd() == pengabdian && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)){
                        fileKinerja = fileKinerja + "\n - " + laporanBkdFile.getNamaFile();
                    }
                }
                cell = new PdfPCell(new Phrase(fileKinerja, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePengabdian.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(pengabdian.getJumlahSks()) + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePengabdian.addCell(cell);


                if(pengabdian.getCatatanAsesor1() != null) {
                    catatan = "- " + pengabdian.getCatatanAsesor1();
                }else{
                    catatan ="-";
                }
                cell = new PdfPCell(new Phrase("Catatan Asesor 1 : \n" + catatan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                tablePengabdian.addCell(cell);

                if(pengabdian.getRekomendasiAsesor1() != null) {
                    rekomendasi = "- " + pengabdian.getRekomendasiAsesor1().toString();
                }else{
                    rekomendasi = "-";
                }
                cell = new PdfPCell(new Phrase("Rekomendasi Asesor 1 : \n" + rekomendasi, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(2);
                tablePengabdian.addCell(cell);

                if(pengabdian.getSksTerpenuhiAsesor1() != null) {
                    sksAsesor1 = pengabdian.getSksTerpenuhiAsesor1().toString();
                }else{
                    sksAsesor1 = "-";
                }
                cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePengabdian.addCell(cell);


                if(pengabdian.getCatatanAsesor2() != null) {
                    catatan = "- " + pengabdian.getCatatanAsesor2();
                }else{
                    catatan = "-";
                }
                cell = new PdfPCell(new Phrase("Catatan Asesor 2 : \n" + catatan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                tablePengabdian.addCell(cell);

                if(pengabdian.getRekomendasiAsesor2() != null) {
                    rekomendasi = "- " + pengabdian.getRekomendasiAsesor2().toString();
                }else{
                    rekomendasi = "-";
                }
                cell = new PdfPCell(new Phrase("Rekomendasi Asesor 2 : \n" + rekomendasi, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(2);
                tablePengabdian.addCell(cell);

                if(pengabdian.getSksTerpenuhiAsesor2() != null) {
                    sksAsesor1 = pengabdian.getSksTerpenuhiAsesor2().toString();
                }else{
                    sksAsesor1 = "-";
                }
                cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePengabdian.addCell(cell);

                cell = new PdfPCell(new Phrase("Catatan Kaprodi : \n", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(6);
                tablePengabdian.addCell(cell);

            }
        }



        document.add(tablePengabdian);


        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENGABDIAN)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)) {
                    String lokasiFile = uploadPenugasan + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Penugasan : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }

        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENGABDIAN)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)) {
                    String lokasiFile = uploadKinerja + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 50));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Kinerja : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }


        //Penunjang=====================================================================================================

        document.newPage();

        document.add(paragraph1);
        paragraph16 = new Paragraph(semester.getNamaSemester(),fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_CENTER);
        paragraph16.setSpacingAfter(10);
        document.add(paragraph16);
        paragraph16 = new Paragraph("Bidang Penunjang", fontTiltle2);
        paragraph16.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph16);

        PdfPTable tablePenunjang = new PdfPTable(7);
        tablePenunjang.setWidthPercentage(100);
        tablePenunjang.setWidths(new int[] {2,3,3,3,3,3,3});
        tablePenunjang.setSpacingBefore(5);

        cell = new PdfPCell(new Phrase("No.",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePenunjang.addCell(cell);
        cell = new PdfPCell(new Phrase("Jenis Kegiatan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePenunjang.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePenunjang.addCell(cell);

        cell = new PdfPCell(new Phrase("Masa Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        tablePenunjang.addCell(cell);

        cell = new PdfPCell(new Phrase("Beban Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        tablePenunjang.addCell(cell);


        cell = new PdfPCell(new Phrase("Bukti Penugasan",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenunjang.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenunjang.addCell(cell);

        cell = new PdfPCell(new Phrase("Bukti Kinerja",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenunjang.addCell(cell);

        cell = new PdfPCell(new Phrase("SKS",font));
        cell.setPadding(5);
        cell.setBackgroundColor(CMYKColor.DARK_GRAY);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePenunjang.addCell(cell);

        a = 0;
        for (LaporanBkd penunjang : laporanBkds) {

            if (penunjang.getBidang().equals(Bidang.PENUNJANG)){
                String catatan = "-";
                String rekomendasi = "-";
                String sksAsesor1 = "-";
                a = a + 1;
                String nomor = a.toString();
                // Adding student id
                cell = new PdfPCell(new Phrase(String.valueOf(a), font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setRowspan(4);
                tablePenunjang.addCell(cell);
                // Adding student name

                cell = new PdfPCell(new Phrase(String.valueOf(penunjang.getNamaKegiatan()), font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePenunjang.addCell(cell);


                String filePenugasan = penunjang.getBuktiPenugasan();
                for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                    if(laporanBkdFile.getLaporanBkd() == penunjang && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)){
                        filePenugasan = filePenugasan + "\n - " + laporanBkdFile.getNamaFile();
                    }
                }

                cell = new PdfPCell(new Phrase(filePenugasan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePenunjang.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(penunjang.getJumlahSks()) + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenunjang.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(penunjang.getMasaPenugasan()) + " Semester", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenunjang.addCell(cell);

                String fileKinerja = penunjang.getBuktiKinerja();
                for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
                    if(laporanBkdFile.getLaporanBkd() == penunjang && laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)){
                        fileKinerja = fileKinerja + "\n - " + laporanBkdFile.getNamaFile();
                    }
                }
                cell = new PdfPCell(new Phrase(fileKinerja, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                tablePenunjang.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(penunjang.getJumlahSks()) + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenunjang.addCell(cell);


                if(penunjang.getCatatanAsesor1() != null) {
                    catatan = "- " + penunjang.getCatatanAsesor1();
                }else{
                    catatan ="-";
                }
                cell = new PdfPCell(new Phrase("Catatan Asesor 1 : \n" + catatan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                tablePenunjang.addCell(cell);

                if(penunjang.getRekomendasiAsesor1() != null) {
                    rekomendasi = "- " + penunjang.getRekomendasiAsesor1().toString();
                }else{
                    rekomendasi = "-";
                }
                cell = new PdfPCell(new Phrase("Rekomendasi Asesor 1 : \n" + rekomendasi, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(2);
                tablePenunjang.addCell(cell);

                if(penunjang.getSksTerpenuhiAsesor1() != null) {
                    sksAsesor1 = penunjang.getSksTerpenuhiAsesor1().toString();
                }else{
                    sksAsesor1 = "-";
                }
                cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenunjang.addCell(cell);


                if(penunjang.getCatatanAsesor2() != null) {
                    catatan = "- " + penunjang.getCatatanAsesor2();
                }else{
                    catatan = "-";
                }
                cell = new PdfPCell(new Phrase("Catatan Asesor 2 : \n" + catatan, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                tablePenunjang.addCell(cell);

                if(penunjang.getRekomendasiAsesor2() != null) {
                    rekomendasi = "- " + penunjang.getRekomendasiAsesor2().toString();
                }else{
                    rekomendasi = "-";
                }
                cell = new PdfPCell(new Phrase("Rekomendasi Asesor 2 : \n" + rekomendasi, font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(2);
                tablePenunjang.addCell(cell);

                if(penunjang.getSksTerpenuhiAsesor2() != null) {
                    sksAsesor1 = penunjang.getSksTerpenuhiAsesor2().toString();
                }else{
                    sksAsesor1 = "-";
                }
                cell = new PdfPCell(new Phrase(sksAsesor1 + " SKS", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablePenunjang.addCell(cell);

                cell = new PdfPCell(new Phrase("Catatan Kaprodi : \n", font2));
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(6);
                tablePenunjang.addCell(cell);

            }
        }

        document.add(tablePenunjang);

        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENUNJANG)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.PENUGASAN)) {
                    String lokasiFile = uploadPenugasan + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Penugasan : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }

        for(LaporanBkdFile laporanBkdFile : laporanBkdFiles){
            if(laporanBkdFile.getLaporanBkd().getBidang().equals(Bidang.PENUNJANG)){
                if(laporanBkdFile.getJenisFileLaporan().equals(JenisFileLaporan.KINERJA)) {
                    String lokasiFile = uploadKinerja + File.separator + laporanBkdFile.getFile();
                    if (laporanBkdFile.getExtensi().equalsIgnoreCase("png") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpg") || laporanBkdFile.getExtensi().equalsIgnoreCase("jpeg")) {
                        Image img = Image.getInstance(lokasiFile);
                        if (img.getWidth() * img.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 50));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        } else {
                            if (img.getWidth() > PageSize.A4.getWidth() - 40) {
                                img.scalePercent((100 / img.getWidth()) * (PageSize.A4.getWidth() - 40));
                            } else {
                                if (img.getHeight() > PageSize.A4.getHeight() - 90) {
                                    img.scalePercent((100 / img.getHeight()) * (PageSize.A4.getHeight() - 90));
                                } else {
                                    img.scalePercent(100);
                                }
                            }
                        }
                        img.setAbsolutePosition(20, PageSize.A4.getHeight() - (img.getScaledHeight()) - 70);
                        document.newPage();
                        paragraphKosong = new Paragraph("File Kinerja : " + laporanBkdFile.getNamaFile());
                        document.add(paragraphKosong);
                        document.add(img);
                    } else if (laporanBkdFile.getExtensi().equalsIgnoreCase("pdf")) {
//                    PdfReader pdfReader = new PdfReader(lokasiFile);

//                    document.newPage(pdfReader.getPdfObject);
                    }

                }else{

                }

            }
        }

        document.close();


    }

}

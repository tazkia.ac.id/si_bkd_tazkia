package id.tazkia.sibkd.konfigurasi;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.SecurityFilterChain;
import org.thymeleaf.extras.springsecurity6.dialect.SpringSecurityDialect;

import id.tazkia.sibkd.dao.config.UserDao;
import id.tazkia.sibkd.entity.config.Permission;
import id.tazkia.sibkd.entity.config.User;

// @EnableWebSecurity
// @EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class KonfigurasiSecurity {

    @Autowired
    private UserDao userDao;


    @Bean @Order(2)
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("/login",
                "/css/**",
                "/bootstrap52/**",
                "/img/**",
                "/normal/**",
                "/",
                "/404",
                "/images/**").permitAll()
                .requestMatchers("/prodi").hasAnyAuthority("MENU_PROGRAM_STUDI")
                .requestMatchers("/prodi/simpan").hasAnyAuthority("MENU_PROGRAM_STUDI")
                .requestMatchers("/prodi/update").hasAnyAuthority("MENU_PROGRAM_STUDI")
                .requestMatchers("/pengguna").hasAnyAuthority("MENU_PENGGUNA")
                .requestMatchers("/pengguna/baru").hasAnyAuthority("MENU_PENGGUNA")
                .requestMatchers("/pengguna/edit").hasAnyAuthority("MENU_PENGGUNA")
                .requestMatchers("/pengguna/simpan").hasAnyAuthority("MENU_PENGGUNA")
                .requestMatchers("/pengguna/hapus").hasAnyAuthority("MENU_PENGGUNA")
                .requestMatchers("/kopertais/pengguna").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
                .requestMatchers("/kopertais/pengguna/baru").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
                .requestMatchers("/kopertais/pengguna/edit").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
                .requestMatchers("/kopertais/pengguna/simpan").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
                .requestMatchers("/kopertais/pengguna/hapus").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
                .requestMatchers("/semester").hasAnyAuthority("MENU_SEMESTER")
                .requestMatchers("/semester/tambah").hasAnyAuthority("MENU_SEMESTER")
                .requestMatchers("/semester/ubah").hasAnyAuthority("MENU_SEMESTER")
                .requestMatchers("/semester/simpan").hasAnyAuthority("MENU_SEMESTER")
                .requestMatchers("/semester/hapus").hasAnyAuthority("MENU_SEMESTER")
                .requestMatchers("/perguruan_tinggi").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
                .requestMatchers("/perguruan_tinggi/baru").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
                .requestMatchers("/perguruan_tinggi/edit").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
                .requestMatchers("/perguruan_tinggi/simpan").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
                .requestMatchers("/perguruan_tinggi/hapus").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
                .requestMatchers("/admin_perguruan_tinggi").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
                .requestMatchers("/admin_perguruan_tinggi/baru").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
                .requestMatchers("/admin_perguruan_tinggi/edit").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
                .requestMatchers("/admin_perguruan_tinggi/simpan").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
                .requestMatchers("/admin_perguruan_tinggi/hapus").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
                .requestMatchers("/dosen").hasAnyAuthority("MENU_DOSEN")
                .requestMatchers("/dosen/baru").hasAnyAuthority("MENU_DOSEN")
                .requestMatchers("/dosen/edit").hasAnyAuthority("MENU_DOSEN")
                .requestMatchers("/dosen/simpan").hasAnyAuthority("MENU_DOSEN")
                .requestMatchers("/dosen/hapus").hasAnyAuthority("MENU_DOSEN")
                .requestMatchers("/jabatan_struktural").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
                .requestMatchers("/jabatan_struktural/baru").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
                .requestMatchers("/jabatan_struktural/edit").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
                .requestMatchers("/jabatan_struktural/simpan").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
                .requestMatchers("/jabatan_struktural/hapus").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
                .requestMatchers("/biodata").hasAnyAuthority("MENU_BIODATA")
                .requestMatchers("/biodata/edit").hasAnyAuthority("MENU_BIODATA")
                .requestMatchers("/biodata/simpan").hasAnyAuthority("MENU_BIODATA")
                .requestMatchers("/kegiatan/pendidikan").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pendidikan/baru").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pendidikan/edit").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pendidikan/simpan").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pendidikan/hapus").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penelitian").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penelitian/baru").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penelitian/edit").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penelitian/simpan").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penelitian/hapus").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pegabdian").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pegabdian/baru").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pegabdian/edit").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pegabdian/simpan").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/pegabdian/hapus").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penunjang").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penunjang/baru").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penunjang/edit").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penunjang/simpan").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/kegiatan/penunjang/hapus").hasAnyAuthority("MENU_KEGIATAN")
                .requestMatchers("/ploting/asesor").hasAnyAuthority("MENU_PLOTING_ASESOR")
                .requestMatchers("/pelaporan_bkd").hasAnyAuthority("MENU_PELAPORAN_BKD")
                .requestMatchers("/pelaporan_bkd/download").hasAnyAuthority("MENU_PELAPORAN_BKD")
                .requestMatchers("/pelaporan_bkd/pelaporanDetail").hasAnyAuthority("MENU_PELAPORAN_BKD")
                .requestMatchers("/bidang/pendidikan").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pendidikan/baru").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pendidikan/edit").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pendidikan/simpan").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pendidikan/hapus").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penelitian").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penelitian/baru").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penelitian/edit").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penelitian/simpan").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penelitian/hapus").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pegabdian").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pegabdian/baru").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pegabdian/edit").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pegabdian/simpan").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/pegabdian/hapus").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penunjang").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penunjang/baru").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penunjang/edit").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penunjang/simpan").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/bidang/penunjang/hapus").hasAnyAuthority("MENU_BIDANG")
                .requestMatchers("/assesment").hasAnyAuthority("MENU_ASSESMENT")
                .requestMatchers("/assesment/detail").hasAnyAuthority("MENU_ASSESMENT")
                .requestMatchers("/assesment/detail/edit").hasAnyAuthority("MENU_ASSESMENT")
                .requestMatchers("/assesment/detail/simpan").hasAnyAuthority("MENU_ASSESMENT")
                .requestMatchers("/rangkuman/bkd").hasAnyAuthority("MENU_RANGKUMAN_BKD")
                .requestMatchers("/assesment/all").hasAnyAuthority("MENU_ASSESMENT_ALL")
                .requestMatchers("/assesment/all/simpan").hasAnyAuthority("MENU_ASSESMENT_ALL")
                        .anyRequest().authenticated()
                )
                .headers().frameOptions().sameOrigin()
                .and().logout().permitAll()
                .and().oauth2Login().loginPage("/login").permitAll()
                .userInfoEndpoint()
                .userAuthoritiesMapper(authoritiesMapper())
                .and().defaultSuccessUrl("/", true);

//        http.securityMatcher("/api/**")
//                .authorizeHttpRequests(auth -> auth
//                        .anyRequest().authenticated()
//                ).oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);
        return http.build();
    }

    // @Override
    // protected void configure(HttpSecurity http) throws Exception {
    //     http
    //             .authorizeRequests()
    //             .antMatchers("/prodi").hasAnyAuthority("MENU_PROGRAM_STUDI")
    //             .antMatchers("/prodi/simpan").hasAnyAuthority("MENU_PROGRAM_STUDI")
    //             .antMatchers("/prodi/update").hasAnyAuthority("MENU_PROGRAM_STUDI")
    //             .antMatchers("/pengguna").hasAnyAuthority("MENU_PENGGUNA")
    //             .antMatchers("/pengguna/baru").hasAnyAuthority("MENU_PENGGUNA")
    //             .antMatchers("/pengguna/edit").hasAnyAuthority("MENU_PENGGUNA")
    //             .antMatchers("/pengguna/simpan").hasAnyAuthority("MENU_PENGGUNA")
    //             .antMatchers("/pengguna/hapus").hasAnyAuthority("MENU_PENGGUNA")
    //             .antMatchers("/kopertais/pengguna").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
    //             .antMatchers("/kopertais/pengguna/baru").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
    //             .antMatchers("/kopertais/pengguna/edit").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
    //             .antMatchers("/kopertais/pengguna/simpan").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
    //             .antMatchers("/kopertais/pengguna/hapus").hasAnyAuthority("MENU_KOPERTAIS_PENGGUNA")
    //             .antMatchers("/semester").hasAnyAuthority("MENU_SEMESTER")
    //             .antMatchers("/semester/tambah").hasAnyAuthority("MENU_SEMESTER")
    //             .antMatchers("/semester/ubah").hasAnyAuthority("MENU_SEMESTER")
    //             .antMatchers("/semester/simpan").hasAnyAuthority("MENU_SEMESTER")
    //             .antMatchers("/semester/hapus").hasAnyAuthority("MENU_SEMESTER")
    //             .antMatchers("/perguruan_tinggi").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
    //             .antMatchers("/perguruan_tinggi/baru").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
    //             .antMatchers("/perguruan_tinggi/edit").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
    //             .antMatchers("/perguruan_tinggi/simpan").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
    //             .antMatchers("/perguruan_tinggi/hapus").hasAnyAuthority("MENU_PERGURUAN_TINGGI")
    //             .antMatchers("/admin_perguruan_tinggi").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
    //             .antMatchers("/admin_perguruan_tinggi/baru").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
    //             .antMatchers("/admin_perguruan_tinggi/edit").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
    //             .antMatchers("/admin_perguruan_tinggi/simpan").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
    //             .antMatchers("/admin_perguruan_tinggi/hapus").hasAnyAuthority("MENU_ADMIN_PERGURUAN_TINGGI")
    //             .antMatchers("/dosen").hasAnyAuthority("MENU_DOSEN")
    //             .antMatchers("/dosen/baru").hasAnyAuthority("MENU_DOSEN")
    //             .antMatchers("/dosen/edit").hasAnyAuthority("MENU_DOSEN")
    //             .antMatchers("/dosen/simpan").hasAnyAuthority("MENU_DOSEN")
    //             .antMatchers("/dosen/hapus").hasAnyAuthority("MENU_DOSEN")
    //             .antMatchers("/jabatan_struktural").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
    //             .antMatchers("/jabatan_struktural/baru").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
    //             .antMatchers("/jabatan_struktural/edit").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
    //             .antMatchers("/jabatan_struktural/simpan").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
    //             .antMatchers("/jabatan_struktural/hapus").hasAnyAuthority("MENU_JABATAN_STRUKTURAL")
    //             .antMatchers("/biodata").hasAnyAuthority("MENU_BIODATA")
    //             .antMatchers("/biodata/edit").hasAnyAuthority("MENU_BIODATA")
    //             .antMatchers("/biodata/simpan").hasAnyAuthority("MENU_BIODATA")
    //             .antMatchers("/kegiatan/pendidikan").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pendidikan/baru").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pendidikan/edit").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pendidikan/simpan").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pendidikan/hapus").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penelitian").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penelitian/baru").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penelitian/edit").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penelitian/simpan").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penelitian/hapus").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pegabdian").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pegabdian/baru").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pegabdian/edit").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pegabdian/simpan").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/pegabdian/hapus").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penunjang").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penunjang/baru").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penunjang/edit").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penunjang/simpan").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/kegiatan/penunjang/hapus").hasAnyAuthority("MENU_KEGIATAN")
    //             .antMatchers("/ploting/asesor").hasAnyAuthority("MENU_PLOTING_ASESOR")
    //             .antMatchers("/pelaporan_bkd").hasAnyAuthority("MENU_PELAPORAN_BKD")
    //             .antMatchers("/pelaporan_bkd/download").hasAnyAuthority("MENU_PELAPORAN_BKD")
    //             .antMatchers("/pelaporan_bkd/pelaporanDetail").hasAnyAuthority("MENU_PELAPORAN_BKD")
    //             .antMatchers("/bidang/pendidikan").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pendidikan/baru").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pendidikan/edit").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pendidikan/simpan").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pendidikan/hapus").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penelitian").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penelitian/baru").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penelitian/edit").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penelitian/simpan").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penelitian/hapus").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pegabdian").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pegabdian/baru").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pegabdian/edit").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pegabdian/simpan").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/pegabdian/hapus").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penunjang").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penunjang/baru").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penunjang/edit").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penunjang/simpan").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/bidang/penunjang/hapus").hasAnyAuthority("MENU_BIDANG")
    //             .antMatchers("/assesment").hasAnyAuthority("MENU_ASSESMENT")
    //             .antMatchers("/assesment/detail").hasAnyAuthority("MENU_ASSESMENT")
    //             .antMatchers("/assesment/detail/edit").hasAnyAuthority("MENU_ASSESMENT")
    //             .antMatchers("/assesment/detail/simpan").hasAnyAuthority("MENU_ASSESMENT")
    //             .antMatchers("/rangkuman/bkd").hasAnyAuthority("MENU_RANGKUMAN_BKD")
    //             .antMatchers("/assesment/all").hasAnyAuthority("MENU_ASSESMENT_ALL")
    //             .antMatchers("/assesment/all/simpan").hasAnyAuthority("MENU_ASSESMENT_ALL")
    //             .anyRequest().authenticated()
    //             .and().logout().permitAll()
    //             .and().oauth2Login().loginPage("/login").permitAll()
    //             .userInfoEndpoint()
    //             .userAuthoritiesMapper(authoritiesMapper())
    //             .and().defaultSuccessUrl("/", true);
    // }

    private GrantedAuthoritiesMapper authoritiesMapper(){
        return (authorities) -> {
            String emailAttrName = "email";
            String email = authorities.stream()
                    .filter(OAuth2UserAuthority.class::isInstance)
                    .map(OAuth2UserAuthority.class::cast)
                    .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                    .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                    .findFirst()
                    .orElse(null);

            if (email == null) {
                return authorities;     // data email tidak ada di userInfo dari Google
            }

            User user = userDao.findByUsername(email);
            if(user == null) {
                throw new IllegalStateException("Email "+email+" tidak ada dalam database");
//                return null;
//                return authorities;     // email user ini belum terdaftar di database
            }

            Set<Permission> userAuthorities = user.getRole().getPermissions();
            if (userAuthorities.isEmpty()) {
                return authorities;     // authorities defaultnya ROLE_USER
            }

            return Stream.concat(
                    authorities.stream(),
                    userAuthorities.stream()
                            .map(Permission::getValue)
                            .map(SimpleGrantedAuthority::new)
            ).collect(Collectors.toCollection(ArrayList::new));
        };
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }

    // @Override
    // public void configure(WebSecurity web) {
    //     web.ignoring()
    //             .antMatchers("/login")
    //             .antMatchers("/css/**")
    //             .antMatchers("/bootstrap52/**")
    //             .antMatchers("/img/**")
    //             .antMatchers("/normal/**")
    //             .antMatchers("/")
    //             .antMatchers("/404")
    //             .antMatchers("/images/**");
    // }




}
